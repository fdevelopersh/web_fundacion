<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas del sitio web
//Home
Route::get('/', 'HomeMainController@ShowHome');

//About
Route::get('/nosotros', 'HomeMainController@ShowAbout');

Route::get('/nosotros/historia', function () {
    return view('others_templates.history');
});

//About-Team
Route::get('/nosotros/equipo/{slug}','HomeMainController@ShowTestimonial');

//Productos
Route::get('/productos','HomeMainController@ShowProducts');

//Productos-Producto
Route::get('/productos/{slug}','HomeMainController@ShowProduct');

//Blog
Route::get('/blog', 'HomeMainController@ShowBlogs');

//Blog-Post
Route::get('/blog/{slug}', 'HomeMainController@ShowBlog');

//Blog-Noticia
Route::get('/blog/noticia1', function () {
    return view('others_templates.blog-news');
});

//Contactanos
Route::get('/contactanos', 'ContactUSController@contactUs');
Route::post('/contactanos/store','ContactUSController@contactUSPost');

Route::get('/galeria','HomeMainController@ShowGallery');


Route::group(['middleware' => ['auth']], function() {
//Admin-Panel
//Dashboard
Route::get('/fwdfundacion/admin-panel', function () {
    return view('manager.dashboard.dashboard');
});

//Users
/* User routes */
Route::get('/fwdfundacion/admin-panel/users', 'UserController@ShowUsers');
Route::get('/fwdfundacion/admin-panel/users/create', 'UserController@CreateUser');
Route::post('/fwdfundacion/admin-panel/users/store', 'UserController@StoreUser');
Route::get('/fwdfundacion/admin-panel/users/edit/{id}', 'UserController@EditUser');
Route::post('/fwdfundacion/admin-panel/users/update/{id}', 'UserController@UpdateUser');
Route::get('/fwdfundacion/admin-panel/users/destroy/{id}', 'UserController@DestroyUser');

//About-Team
Route::get('/fwdfundacion/admin-panel/about/team', 'TeamController@ShowTeam');
Route::get('/fwdfundacion/admin-panel/about/team/create', 'TeamController@CreateTeam');
Route::post('/fwdfundacion/admin-panel/about/team/store', 'TeamController@StoreTeam');
Route::get('/fwdfundacion/admin-panel/about/team/edit/{id}', 'TeamController@EditTeam');
Route::post('/fwdfundacion/admin-panel/about/team/update/{id}', 'TeamController@UpdateTeam');
Route::get('/fwdfundacion/admin-panel/about/team/destroy/{id}', 'TeamController@DestroyTeam');

//Products
Route::get('/fwdfundacion/admin-panel/products', 'ProductController@ShowProducts');
Route::get('/fwdfundacion/admin-panel/products/create', 'ProductController@CreateProduct');
Route::post('/fwdfundacion/admin-panel/products/store', 'ProductController@StoreProduct');
Route::get('/fwdfundacion/admin-panel/products/edit/{id}', 'ProductController@EditProduct');
Route::post('/fwdfundacion/admin-panel/products/update/{id}', 'ProductController@UpdateProduct');
Route::get('/fwdfundacion/admin-panel/products/destroy/{id}', 'ProductController@DestroyProduct');

//Blog
Route::get('/fwdfundacion/admin-panel/blog', 'BlogController@ShowBlogs');
Route::get('/fwdfundacion/admin-panel/blog/create', 'BlogController@CreateBlog');
Route::post('/fwdfundacion/admin-panel/blog/store', 'BlogController@StoreBlog');
Route::get('/fwdfundacion/admin-panel/blog/edit/{id}', 'BlogController@EditBlog');
Route::post('/fwdfundacion/admin-panel/blog/update/{id}', 'BlogController@UpdateBlog');
Route::get('/fwdfundacion/admin-panel/blog/destroy/{id}', 'BlogController@DestroyBlog');

//Galeria-Imagenes
Route::get('/fwdfundacion/admin-panel/galeria/imagenes', 'ImageController@ShowImages');
Route::get('/fwdfundacion/admin-panel/galeria/imagenes/create', 'ImageController@CreateImage');
Route::post('/fwdfundacion/admin-panel/galeria/imagenes/store', 'ImageController@StoreImage');
Route::get('/fwdfundacion/admin-panel/galeria/imagenes/edit/{id}', 'ImageController@EditImage');
Route::post('/fwdfundacion/admin-panel/galeria/imagenes/update/{id}', 'ImageController@UpdateImage');
Route::get('/fwdfundacion/admin-panel/galeria/imagenes/destroy/{id}', 'ImageController@DestroyImage');

//Galeria-Videos
Route::get('/fwdfundacion/admin-panel/galeria/videos', 'VideoController@ShowVideos');
Route::get('/fwdfundacion/admin-panel/galeria/videos/create', 'VideoController@CreateVideo');
Route::post('/fwdfundacion/admin-panel/galeria/videos/store', 'VideoController@StoreVideo');
Route::get('/fwdfundacion/admin-panel/galeria/videos/edit/{id}', 'VideoController@EditVideo');
Route::post('/fwdfundacion/admin-panel/galeria/videos/update/{id}', 'VideoController@UpdateVideo');
Route::get('/fwdfundacion/admin-panel/galeria/videos/destroy/{id}', 'VideoController@DestroyVideo');

//Home-Slider
Route::get('/fwdfundacion/admin-panel/slider', 'SliderController@ShowSliders');
Route::get('/fwdfundacion/admin-panel/sliders/create', 'SliderController@CreateSlider');
Route::post('/fwdfundacion/admin-panel/sliders/store', 'SliderController@StoreSlider');
Route::get('/fwdfundacion/admin-panel/sliders/edit/{id}', 'SliderController@EditSlider');
Route::post('/fwdfundacion/admin-panel/sliders/update/{id}', 'SliderController@UpdateSlider');
Route::get('/fwdfundacion/admin-panel/sliders/destroy/{id}', 'SliderController@DestroySlider');

//Home-Testimonials
Route::get('/fwdfundacion/admin-panel/clientes-testimonios', 'TestimonialController@ShowTestimonials');
Route::get('/fwdfundacion/admin-panel/clientes-testimonios/create', 'TestimonialController@CreateTestimonial');
Route::post('/fwdfundacion/admin-panel/clientes-testimonios/store', 'TestimonialController@StoreTestimonial');
Route::get('/fwdfundacion/admin-panel/clientes-testimonios/edit/{id}', 'TestimonialController@EditTestimonial');
Route::post('/fwdfundacion/admin-panel/clientes-testimonios/update/{id}', 'TestimonialController@UpdateTestimonial');
Route::get('/fwdfundacion/admin-panel/clientes-testimonios/destroy/{id}', 'TestimonialController@DestroyTestimonial');

//Home-Patrocinadores
Route::get('/fwdfundacion/admin-panel/patrocinadores', 'PartnerController@ShowPartners');
Route::get('/fwdfundacion/admin-panel/patrocinadores/create', 'PartnerController@CreatePartner');
Route::post('/fwdfundacion/admin-panel/patrocinadores/store', 'PartnerController@StorePartner');
Route::get('/fwdfundacion/admin-panel/patrocinadores/edit/{id}', 'PartnerController@EditPartner');
Route::post('/fwdfundacion/admin-panel/patrocinadores/update/{id}', 'PartnerController@UpdatePartner');
Route::get('/fwdfundacion/admin-panel/patrocinadores/destroy/{id}', 'PartnerController@DestroyPartner');


});
// Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('/admin/login', 'Auth\LoginController@login');
// Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/fwdfundacion/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/fwdfundacion/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//Auth::routes();

//Route::get('/admin-panel', 'HomeController@index')->name('admin-panel');
