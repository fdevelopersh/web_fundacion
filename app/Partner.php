<?php

namespace fpmoises;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Partner extends Authenticatable
{
  use HasSlug;
    use Notifiable;
    protected $table = 'partners';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name','img_partner','img_description',
  ];

  public function getSlugOptions() : SlugOptions
  {
    return SlugOptions::create()
    ->generateSlugsFrom('name')
    ->saveSlugsTo('slug');
  }

  public function scopeName($query,$name){
    if (trim($name) == "") {
    }else
    $query->where('name',"LIKE","%$name%");
  }
}
