<?php

namespace fpmoises\Http\Controllers;

use Illuminate\Http\Request;
use fpmoises\Http\Requests;
use fpmoises\Models\ContactUS;
use Mail;

class ContactUSController extends Controller
{
  public function contactUs()
 {
     return view('contact');
 }

 public function contactUSPost(Request $request)
  {
      $contactus = new ContactUS;

      $this->validate($request,[
        'name' => 'required',
        'telephone' => 'required',
        'email' => 'required',
        'comment' => 'required',
        'company' => 'required',
        'g-recaptcha-response' => 'required|recaptchav3:contacto,0.5',
      ]);

      $contactus->name = $request->input('name');
      $contactus->telephone = $request->input('telephone');
      $contactus->email = $request->input('email');
      $contactus->comment = $request->input('comment');
      $contactus->company= $request->input('company');

      $contactus->save();

      Mail::send('email',
       array(
           'name' => $request->get('name'),
           'telephone' => $request->get('telephone'),
           'email' => $request->get('email'),
           'company' => $request->get('company'),
           'comment' => $request->get('comment')
       ), function($message)
   {
       $message->from('fdevelopershn@gmail.com');
       $message->to('fdevelopershn@gmail.com', 'Admin')->subject('Contacts');
   });

    return redirect(action('ContactUSController@contactUs'))
    ->with('success','Gracias por contactarnos!');

  }

}
