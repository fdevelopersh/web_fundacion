<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowProducts(Request $request)
    {
            $products = Product::Name($request->get('name'))
            ->select('products.id','products.title','products.content_title'
            ,'products.content_product','products.image_product'
            )->get();

            return view('manager.products.show_product',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateProduct()
    {
        return view('manager.products.create_product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreProduct(Request $request)
    {
      $product = new Product();

      $product->title = $request->input('title');
      $product->content_title = $request->input('content_title');
      $product->image_alt = $request->input('image_alt');
      $product->content_product= $request->input('editor_content');


        /* Validation of fields */
        $this->validate($request,[
            'title' => 'required',
            'content_title' => 'required',
            'editor_content' => 'required',
            'image_alt' => 'required',
            'image_product' => 'required',
          ]);

        $file_product_image = $request->file('image_product');
        $destinationPath= 'img/products';
        $image_product_extension = $request->file('image_product')->getClientOriginalExtension();
        $image = $product->image_alt.".".$image_product_extension;
        $product->image_product = $image;
        $file_product_image->move($destinationPath, $image);


        $product->save();
        return redirect(action('ProductController@ShowProducts'))
        ->with('success','El producto "'.  $product->title . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditProduct($id)
    {
        $product = Product::find($id);

        return view('manager.products.edit_product',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateProduct(Request $request, $id)
    {
        $product = Product::Find($id);

        $product->title = $request->input('title');
        $product->content_title = $request->input('content_title');
        $product->image_alt = $request->input('image_alt');
        $product->content_product= $request->input('editor_content');


          /* Validation of fields */
          $this->validate($request,[
              'title' => 'required',
              'content_title' => 'required',
              'image_alt' => 'required',
              'editor_content' => 'required',
            ]);

        if ($request->file('image_product')=="") {

        }else {
          File::Delete('img/products/'.$product->image_product);
          $file_product_image = $request->file('image_product');
          $destinationPath= 'img/products';
          $image_product_extension = $request->file('image_product')->getClientOriginalExtension();
          $image = $product->image_alt.".".$image_product_extension;
          $product->image_product = $image;
          $file_product_image->move($destinationPath, $image);
        }

        $product->save();
        return redirect(action('ProductController@ShowProducts'))
        ->with('success','El producto "'.  $product->title . '" ha sido editado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyProduct($id)
    {
        $product = Product::find($id);
        $product->delete();

        File::Delete('img/products/'.$product->image_product);

        return redirect(action('ProductController@ShowProducts'))
        ->with('danger','El producto "'.  $product->title . '" ha sido eliminado');
    }
}
