<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Models\Team;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowTeam(Request $request)
    {
            $teams = Team::Name($request->get('name'))
            ->select('team.id','team.name_team','team.job_title','team.image'
            )->get();

            return view('manager.about.team.show_team',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateTeam()
    {
        return view('manager.about.team.create_team');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreTeam(Request $request)
    {
      $team = new Team();

      $team->name_team = $request->input('name_team');
      $team->job_title = $request->input('job_title');
      $team->greeting_title = $request->input('greeting_title');
      $team->phrase = $request->input('phrase');
      $team->image_description = $request->input('image_description');
      $team->testimonial = $request->input('editor_content');

        /* Validation of fields */
        $this->validate($request,[
            'name_team' => 'required',
            'job_title' => 'required',
            'greeting_title' => 'required',
            'phrase' => 'required',
          ]);

      if ($request->file('image')=="") {
        $team->image = "default.jpg";
      }else {
        $file_team_image = $request->file('image');
        $destinationPath= 'img/team';
        $image_team_extension = $request->file('image')->getClientOriginalExtension();
        $image = $team->image_description.".".$image_team_extension;
        $team->image = $image;
        $file_team_image->move($destinationPath, $image);
      }

        $team->save();
        return redirect(action('TeamController@ShowTeam'))
        ->with('success','El miembro "'.  $team->name_team . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditTeam($id)
    {
        $team = Team::find($id);

        return view('manager.about.team.edit_team',compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateTeam(Request $request, $id)
    {
        $team = Team::Find($id);

        $team->name_team = $request->input('name_team');
        $team->job_title = $request->input('job_title');
        $team->greeting_title = $request->input('greeting_title');
        $team->phrase = $request->input('phrase');
        $team->image_description = $request->input('image_description');
        $team->testimonial = $request->input('editor_content');

          /* Validation of fields */
          $this->validate($request,[
              'name_team' => 'required',
              'job_title' => 'required',
              'greeting_title' => 'required',
              'phrase' => 'required',
            ]);

        if ($request->file('image')=="") {

        }else {
          File::Delete('img/team/'.$team->image);
          $file_team_image = $request->file('image');
          $destinationPath= 'img/team';
          $image_team_extension = $request->file('image')->getClientOriginalExtension();
          $image = $team->image_description.".".$image_team_extension;
          $team->image = $image;
          $file_team_image->move($destinationPath, $image);
        }

          $team->save();
          return redirect(action('TeamController@ShowTeam'))
          ->with('success','El miembro "'.  $team->name_team . '" ha sido editado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyTeam($id)
    {
        $team = Team::find($id);
        $team->delete();
        if ($team->image=="default.jpg") {
          # code...
        }else{
          File::Delete('img/team/'.$team->image);
        }

        return redirect(action('TeamController@ShowTeam'))
        ->with('danger','El Miembro "'.  $team->name_name . '" ha sido eliminado');
    }
}
