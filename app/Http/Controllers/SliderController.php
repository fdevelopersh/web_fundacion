<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Slider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowSliders(Request $request)
    {

         // if (Auth::user()->id_role!=1) {
            $sliders = Slider::Name($request->get('name'))
            ->select('sliders.id','sliders.title','sliders.img_slider')->get();

      return view('manager.home.slider.show_slider',compact('sliders'));
        // }
        // else {
        //   return redirect('/admin/main_start')
        // ->with('danger','You need administrator privileges');
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateSlider()
    {
        return view('manager.home.slider.create_slider');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreSlider(Request $request)
    {
      $slider = new Slider();

      $slider->title = $request->input('title');
      $slider->subtitle = $request->input('subtitle');
      $slider->img_alt = $request->input('img_alt');
      $slider->align_text = $request->input('align_text');

        /* Validation of fields */
        $this->validate($request,[
            'title' => 'required',
            'subtitle' => 'required',
            'image_slider' => 'required',
            'align_text' => 'required',
            'img_alt' => 'required',
          ]);

          if ($request->file('image_slider')=="") {
            $slider->img_slider = "default.png";
          }else {
            $file_slider_image = $request->file('image_slider');
            $destinationPathB= 'img/slider';
            $image_slider_extension = $request->file('image_slider')->getClientOriginalExtension();
            $image_slider = $slider->img_alt.".".$image_slider_extension;
            $slider->img_slider = $image_slider;
            $file_slider_image->move($destinationPathB, $image_slider);
          }

        $slider->save();

        return redirect(action('SliderController@ShowSliders'))
        ->with('success','La imagen "'.  $slider->title . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditSlider($id)
    {
        $slider = Slider::find($id);

        return view('manager.home.slider.edit_slider',compact('slider'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateSlider(Request $request, $id)
    {
        $slider = Slider::find($id);

        $slider->title = $request->input('title');
        $slider->subtitle = $request->input('subtitle');
        $slider->img_alt = $request->input('img_alt');
        $slider->align_text = $request->input('align_text');

          /* Validation of fields */
          $this->validate($request,[
              'title' => 'required',
              'subtitle' => 'required',
              'align_text' => 'required',
              'img_alt' => 'required',
            ]);

            if ($request->file('image_slider')=="") {

            }else {
              File::Delete('img/slider/'.$slider->img_slider);
              $file_slider_image = $request->file('image_slider');
              $destinationPathB= 'img/slider';
              $image_slider_extension = $request->file('image_slider')->getClientOriginalExtension();
              $image_slider = $slider->img_alt.".".$image_slider_extension;
              $slider->img_slider = $image_slider;
              $file_slider_image->move($destinationPathB, $image_slider);
            }

          $slider->save();
          return redirect(action('SliderController@ShowSliders'))
          ->with('success','El slider "'.  $slider->title . '" ha sido editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroySlider($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        File::Delete('img/slider/'.$slider->img_slider);

        return redirect(action('SliderController@ShowSliders'))
        ->with('danger','El Slide "'.  $slider->title . '" ha sido eliminado');
    }
}
