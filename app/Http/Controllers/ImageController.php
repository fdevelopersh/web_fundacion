<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Models\Img;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowImages(Request $request)
    {     
     $images = Img::Name($request->get('name'))
     ->select('images.id','images.image_text','images.image_alt','images.image')->get();

      return view('manager.gallery.images.show_image',compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateImage()
    {
        return view('manager.gallery.images.create_image');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreImage(Request $request)
    {
      $image = new Img();

      $image->image_text = $request->input('image_text');
      $image->image_alt = $request->input('image_alt');


        /* Validation of fields */
        $this->validate($request,[
            'image_text' => 'required',
            'image_alt' => 'required',
            'image' => 'required',
          ]);
   
            $file_image = $request->file('image');
            $destinationPathB= 'img/gallery/images';
            $image_extension = $request->file('image')->getClientOriginalExtension();
            $image_name = uniqid()."-".$image->image_text.".".$image_extension;
            $image->image = $image_name;
            $file_image->move($destinationPathB, $image_name);

        $image->save();

        return redirect(action('ImageController@ShowImages'))
        ->with('success','La imagen "'. $image->image_text . '" ha sido creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditImage($id)
    {
        $image = Img::find($id);

        return view('manager.gallery.images.edit_image',compact('image'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateImage(Request $request, $id)
    {
        $image = Img::find($id);

        $image->image_text = $request->input('image_text');
        $image->image_alt = $request->input('image_alt');
  
  
          /* Validation of fields */
          $this->validate($request,[
              'image_text' => 'required',
              'image_alt' => 'required',
            ]);
     
            if ($request->file('image')=="") {

            }else {
              File::Delete('img/gallery/images/'.$image->image);
              $file_image = $request->file('image');
              $destinationPathB= 'img/gallery/images';
              $image_extension = $request->file('image')->getClientOriginalExtension();
              $image_name = uniqid()."-".$image->image_text.".".$image_extension;
              $image->image = $image_name;
              $file_image->move($destinationPathB, $image_name);
            }

          $image->save();
  
          return redirect(action('ImageController@ShowImages'))
          ->with('success','La imagen "'. $image->image_text . '" ha sido editada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyImage($id)
    {
        $image = Img::find($id);
        $image->delete();
        File::Delete('img/gallery/images/'.$image->image);

        return redirect(action('ImageController@ShowImages'))
        ->with('danger','La imagen "'. $image->image_text . '" ha sido eliminada');
    }
}
