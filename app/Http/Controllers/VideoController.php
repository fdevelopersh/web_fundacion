<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Models\Video;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowVideos(Request $request)
    {     
     $videos = Video::Name($request->get('name'))
     ->select('videos.id','videos.name_video','videos.link_video')->get();

      return view('manager.gallery.videos.show_video',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateVideo()
    {
        return view('manager.gallery.videos.create_video');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreVideo(Request $request)
    {
      $video = new Video();

      $video->name_video = $request->input('name_video');
      $video->link_video = $request->input('link_video');

        /* Validation of fields */
        $this->validate($request,[
            'name_video' => 'required',
            'link_video' => 'required',
          ]);

        $video->save();

        return redirect(action('VideoController@ShowVideos'))
        ->with('success','El video "'. $video->name_video . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditVideo($id)
    {
        $video = Video::find($id);

        return view('manager.gallery.videos.edit_video',compact('video'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateVideo(Request $request, $id)
    {
        $video = Video::find($id);

        $video->name_video = $request->input('name_video');
        $video->link_video = $request->input('link_video');

        /* Validation of fields */
        $this->validate($request,[
            'name_video' => 'required',
            'link_video' => 'required',
          ]);

        $video->save();

        return redirect(action('VideoController@ShowVideos'))
        ->with('success','El video "'. $video->name_video . '" ha sido editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyVideo($id)
    {
        $video= Video::find($id);
        $video->delete();
        File::Delete('img/gallery/videos/'.$video->link_video);

        return redirect(action('VideoController@ShowVideos'))
        ->with('danger','El video "'. $video->link_video . '" ha sido eliminado');
    }
}
