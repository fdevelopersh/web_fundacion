<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Testimonial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowTestimonials(Request $request)
    {

         // if (Auth::user()->id_role!=1) {
            $tests = Testimonial::Name($request->get('name'))
            ->select('customers_testimonial.id','customers_testimonial.name','customers_testimonial.job_title','customers_testimonial.img_testimonial')->get();

      return view('manager.home.customers-testimonial.show_customers_testimonial',compact('tests'));
        // }
        // else {
        //   return redirect('/admin/main_start')
        // ->with('danger','You need administrator privileges');
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateTestimonial()
    {
        return view('manager.home.customers-testimonial.create_customers_testimonial');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreTestimonial(Request $request)
    {
      $testimonial = new Testimonial();

      $testimonial->name = $request->input('name');
      $testimonial->job_title = $request->input('job_title');
      $testimonial->content = $request->input('content');
      $testimonial->img_testimonial = $request->input('image_testimonial');

        /* Validation of fields */
        $this->validate($request,[
            'name' => 'required',
            'job_title' => 'required',
            'content' => 'required',
            'image_testimonial' => 'required',
          ]);

          if ($request->file('image_testimonial')=="") {
            $testimonial->img_testimonial = "default.png";
          }else {
            $file_testimonial_image = $request->file('image_testimonial');
            $destinationPathB= 'img/customers-testimonial';
            $image_testimonial_extension = $request->file('image_testimonial')->getClientOriginalExtension();
            $image_testimonial = uniqid()."-".$testimonial->name.".".$image_testimonial_extension;
            $testimonial->img_testimonial = $image_testimonial;
            $file_testimonial_image->move($destinationPathB, $image_testimonial);
          }

        $testimonial->save();

        return redirect(action('TestimonialController@ShowTestimonials'))
        ->with('success','El testimonio de "'.  $testimonial->name . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditTestimonial($id)
    {
        $testimonial = Testimonial::find($id);

        return view('manager.home.customers-testimonial.edit_customers_testimonial',compact('testimonial'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateTestimonial(Request $request, $id)
    {
        $testimonial = Testimonial::find($id);

        $testimonial->name = $request->input('name');
        $testimonial->job_title = $request->input('job_title');
        $testimonial->content = $request->input('content');

          /* Validation of fields */
          $this->validate($request,[
              'name' => 'required',
              'job_title' => 'required',
              'content' => 'required',
            ]);

            if ($request->file('image_testimonial')=="") {

            }else {
              File::Delete('img/customers-testimonial/'.$testimonial->img_testimonial);
              $file_testimonial_image = $request->file('image_testimonial');
              $destinationPathB= 'img/customers-testimonial';
              $image_testimonial_extension = $request->file('image_testimonial')->getClientOriginalExtension();
              $image_testimonial = uniqid()."-".$testimonial->name.".".$image_testimonial_extension;
              $testimonial->img_testimonial = $image_testimonial;
              $file_testimonial_image->move($destinationPathB, $image_testimonial);
            }

          $testimonial->save();
          return redirect(action('TestimonialController@ShowTestimonials'))
          ->with('success','El testimonio de "'.  $testimonial->name . '" ha sido editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyTestimonial($id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->delete();
        File::Delete('img/customers-testimonial/'.$testimonial->img_testimonial);

        return redirect(action('TestimonialController@ShowTestimonials'))
        ->with('danger','El testimonio de "'.  $testimonial->name . '" ha sido eliminado');
    }
}
