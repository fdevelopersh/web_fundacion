<?php

namespace fpmoises\Http\Controllers;

use fpmoises\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowUsers(Request $request)
    {

        if (Auth::user()->type_user==1) {
            $users = User::Name($request->get('name'))
            ->select('users.id','users.name','users.email','users.type_user'
            ,'users.image_user')->get();
            return view('manager.users.show_user',compact('users'));
        }
        else {
        return redirect('/fwdfundacion/admin-panel')
        ->with('warning','Necesitas permisos de Administrador');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateUser()
    {
      if (Auth::user()->type_user==1) {
        return view('manager.users.create_user');
      }
      else {
      return redirect('/fwdfundacion/admin-panel')
      ->with('warning','Necesitas permisos de Administrador');
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreUser(Request $request)
    {
      if (Auth::user()->type_user==1) {
      $user = new User();

      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->type_user = $request->input('type_user');

        /* Validation of fields */
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'type_user' => 'required',
            'password' => 'required|min:8',
          ]);

      if ($request->file('image_user')=="") {
        $user->image_user = "default.png";
      }else {
        $file_user_image = $request->file('image_user');
        $destinationPath= 'img/users';
        $image_user_extension = $request->file('image_user')->getClientOriginalExtension();
        $image = uniqid()."-".$user->name.".".$image_user_extension;
        $user->image_user = $image;
        $file_user_image->move($destinationPath, $image);
      }

      if ($request->file('image_background')=="") {
        $user->image_background = "default.jpg";
      }else {
        $file_user_image = $request->file('image_background');
        $destinationPathB= 'img/background';
        $image_background_extension = $request->file('image_background')->getClientOriginalExtension();
        $image_background = uniqid()."-background-".$user->name.".".$image_background_extension;
        $user->image_background = $image_background;
        $file_user_image->move($destinationPathB, $image_background);
      }

        $user->password = bcrypt($request->input('password'));
        $user->save();
        return redirect(action('UserController@ShowUsers'))
        ->with('success','El usuario "'.  $user->name . '" ha sido creado exitosamente');
      }
      else {
      return redirect('/fwdfundacion/admin-panel')
      ->with('warning','Necesitas permisos de Administrador');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditUser($id)
    {
      if (Auth::user()->type_user==1) {
        $user = User::find($id);
      }else{
        $user = User::find(Auth::user()->id);
      }

      return view('manager.users.edit_user',compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateUser(Request $request, $id)
    {
      if (Auth::user()->type_user==1) {
        $user = User::find($id);
        $user->type_user = $request->input('type_user');
      }elseif(Auth::user()->type_user==2){
        $user = User::find(Auth::user()->id);
      }


        $user->name = $request->input('name');
        $user->email = $request->input('email');


            /* Validation of fields */
          $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
          ]);


        if ($request->file('image_user')=="") {
        }else {
          File::Delete('img/users/'.$user->image_user);
          $file_user_image = $request->file('image_user');
          $destinationPath= 'img/users';
          $image_user_extension = $request->file('image_user')->getClientOriginalExtension();
          $image = uniqid()."-".$user->name.".".$image_user_extension;
          $user->image_user = $image;
          $file_user_image->move($destinationPath, $image);
        }

        if ($request->file('image_background')=="") {
        }else {
          File::Delete('img/background/'.$user->image_background);
          $file_user_image = $request->file('image_background');
          $destinationPathB= 'img/background';
          $image_background_extension = $request->file('image_background')->getClientOriginalExtension();
          $image_background = uniqid()."-background-".$user->name.".".$image_background_extension;
          $user->image_background = $image_background;
          $file_user_image->move($destinationPathB, $image_background);
        }

          if($request->input('password')==""){
          }else{
            $this->validate($request,[
                'password' => 'required|min:8',
              ]);
            $user->password = bcrypt($request->input('password'));
          }
          $user->save();

          if (Auth::user()->id==$id) {
            return redirect('/fwdfundacion/admin-panel')
            ->with('success','Perfil actualizado exitosamente');
          }else{
          return redirect(action('UserController@ShowUsers'))
          ->with('success','El usuario "'.  $user->name . '" ha sido editado');
          }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyUser($id)
    {
      if (Auth::user()->type_user==1) {
        $user = User::find($id);
      }else{
        $user = User::find(Auth::user()->id);
      }
        $user->delete();
        if ($user->image_user=="default.png") {
          # code...
        }else{
          File::Delete('img/users/'.$user->image_user);
        }

        if ($user->image_background=="default.jpg") {
            # code...
          }else{
            File::Delete('img/background/'.$user->image_background);
          }
        return redirect(action('UserController@ShowUsers'))
        ->with('danger','El Usuario "'.  $user->name . '" ha sido eliminado exitosamente');
    }
}
