<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Slider;
use fpmoises\Partner;
use fpmoises\Testimonial;
use fpmoises\Models\Blog;
use fpmoises\Models\Img;
use fpmoises\Models\Team;
use fpmoises\Models\Product;
use fpmoises\Models\Video;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class HomeMainController extends Controller
{
    public function ShowHome(Request $request)
    {

    $sliders = Slider::all();
    $partners = Partner::all();
    $testimonials = Testimonial::all();
    $blogs = Blog::orderBy('id', 'desc')->paginate(2);
    Date::setLocale('es');

    return view('home',compact('sliders','partners','testimonials','blogs'));

    }

    public function ShowAbout(Request $request)
    {
        $teams = Team::all();

        return view('about',compact('teams'));
    }

    public function ShowTestimonial($slug)
    {
        $team = Team::whereSlug($slug)->first();

        return view('others_templates.testimonials-team',compact('team'));
    }

    public function ShowProducts(Request $request)
    {

    $products = Product::all();

    return view('products',compact('products'));

    }

    public function ShowProduct($slug)
    {

    $product = Product::whereSlug($slug)->first();;

    return view('others_templates.products-information',compact('product'));

    }

    public function ShowBlogs(Request $request)
    {

    $blogs = Blog::orderBy('id', 'desc')->paginate(4);
    Date::setLocale('es');

    return view('blog',compact('blogs','total','page'));

    }

    public function ShowBlog($slug)
    {

    $blog = Blog::whereSlug($slug)->first();
    Date::setLocale('es');

    return view('others_templates.blog-news',compact('blog'));

    }

    public function ShowGallery(Request $request)
    {

    $images = Img::all();
    $videos = Video::all();

    return view('gallery',compact('images','videos'));

    }
}
