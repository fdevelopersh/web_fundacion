<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Partner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowPartners(Request $request)
    {

         // if (Auth::user()->id_role!=1) {
            $partners = Partner::Name($request->get('name'))
            ->select('partners.id','partners.name','partners.img_description','partners.img_partner')->get();

      return view('manager.home.partners.show_partners',compact('partners'));
        // }
        // else {
        //   return redirect('/admin/main_start')
        // ->with('danger','You need administrator privileges');
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreatePartner()
    {
        return view('manager.home.partners.create_partners');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StorePartner(Request $request)
    {
      $partner = new Partner();

      $partner->name = $request->input('name');
      $partner->img_description = $request->input('description');
      $partner->img_partner = $request->input('image_partner');

        /* Validation of fields */
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'image_partner' => 'required',
          ]);

          if ($request->file('image_partner')=="") {
            $partner->img_partner = "default.png";
          }else {
            $file_partner_image = $request->file('image_partner');
            $destinationPathB= 'img/partners';
            $image_partner_extension = $request->file('image_partner')->getClientOriginalExtension();
            $image_partner = uniqid()."-".$partner->name.".".$image_partner_extension;
            $partner->img_partner = $image_partner;
            $file_partner_image->move($destinationPathB, $image_partner);
          }

        $partner->save();

        return redirect(action('PartnerController@ShowPartners'))
        ->with('success','El patrocinador "'.  $partner->name . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditPartner($id)
    {
        $partner = Partner::find($id);

        return view('manager.home.partners.edit_partners',compact('partner'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdatePartner(Request $request, $id)
    {
        $partner = Partner::find($id);

        $partner->name = $request->input('name');
        $partner->img_description = $request->input('description');

          /* Validation of fields */
          $this->validate($request,[
              'name' => 'required',
              'description' => 'required',
            ]);

            if ($request->file('image_partner')=="") {

            }else {
              File::Delete('img/partners/'.$partner->img_partner);
              $file_partner_image = $request->file('image_partner');
              $destinationPathB= 'img/partners';
              $image_partner_extension = $request->file('image_partner')->getClientOriginalExtension();
              $image_partner = uniqid()."-".$partner->name.".".$image_partner_extension;
              $partner->img_partner = $image_partner;
              $file_partner_image->move($destinationPathB, $image_partner);
            }

          $partner->save();
          return redirect(action('PartnerController@ShowPartners'))
          ->with('success','El patrocinador "'.  $partner->name . '" ha sido editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyPartner($id)
    {
        $partner = Partner::find($id);
        $partner->delete();
        File::Delete('img/partners/'.$partner->img_partner);

        return redirect(action('PartnerController@ShowPartners'))
        ->with('danger','El patrocinador "'.  $partner->name . '" ha sido eliminado');
    }
}
