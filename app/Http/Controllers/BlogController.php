<?php

namespace fpmoises\Http\Controllers;

use fpmoises\Models\Blog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowBlogs(Request $request)
    {
          $blogs = Blog::Name($request->get('name'))
          ->select('blogs.id','blogs.title','blogs.subtitle','blogs.image','blogs.image_alt')->get();

          return view('manager.blog.show_blog',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateBlog()
    {
        return view('manager.blog.create_blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreBlog(Request $request)
    {
      $blog = new Blog();

      $blog->title = $request->input('title');
      $blog->subtitle = $request->input('subtitle');
      $blog->image_alt = $request->input('image_alt');
      $blog->content = $request->input('editor_content');
        /* Validation of fields */
        $this->validate($request,[
            'title' => 'required',
            'subtitle' => 'required',
            'editor_content' => 'required',
            'image_alt' =>'required',
            'image_blog' => 'required',
          ]);

        $file_blog_image = $request->file('image_blog');
        $destinationPath= 'img/blog';
        $image_blog_extension = $request->file('image_blog')->getClientOriginalExtension();
        $image = $blog->image_alt.".".$image_blog_extension;
        $blog->image = $image;
        $file_blog_image->move($destinationPath, $image);

        $blog->save();

        return redirect(action('BlogController@ShowBlogs'))
        ->with('success','El post "'.  $blog->title . '" ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditBlog($id)
    {
        $blog = Blog::find($id);

        return view('manager.blog.edit_blog',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateBlog(Request $request, $id)
    {
        $blog = Blog::Find($id);

        $blog->title = $request->input('title');
        $blog->subtitle = $request->input('subtitle');
        $blog->image_alt = $request->input('image_alt');
        $blog->content = $request->input('editor_content');
          /* Validation of fields */
          $this->validate($request,[
              'title' => 'required',
              'subtitle' => 'required',
              'editor_content' => 'required',
              'image_alt' =>'required',
            ]);

        if ($request->file('image_blog')=="") {

        }else {
          File::Delete('img/blog/'.$blog->image);
          $file_blog_image = $request->file('image_blog');
          $destinationPath= 'img/blog';
          $image_blog_extension = $request->file('image_blog')->getClientOriginalExtension();
          $image = $blog->image_alt.".".$image_blog_extension;
          $blog->image = $image;
          $file_blog_image->move($destinationPath, $image);
        }

        $blog->save();

        return redirect(action('BlogController@ShowBlogs'))
        ->with('success','El post "'.  $blog->title . '" ha sido editado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyBlog($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        File::Delete('img/blog/'.$blog->image);

        return redirect(action('BlogController@ShowBlogs'))
        ->with('danger','El post "'.  $blog->title . '" ha sido eliminado');
    }
}
