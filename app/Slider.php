<?php

namespace fpmoises;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Slider extends Authenticatable
{
    use Notifiable;
    protected $table = 'sliders';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title','subtitle','img_slider','align_text','img_alt',
  ];

  public function scopeName($query,$name){
    if (trim($name) == "") {
    }else
    $query->where('title',"LIKE","%$name%");
  }
}
