<?php

namespace fpmoises\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasSlug;
    protected $table = 'products';

    public function getSlugOptions() : SlugOptions
    {
    	return SlugOptions::create()
    	->generateSlugsFrom('title')
    	->saveSlugsTo('slug');
    }

    public function scopeName($query,$name){
        if (trim($name) == "") {
        }else
        $query->where('title',"LIKE","%$name%");
      }
}
