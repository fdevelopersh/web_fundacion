<?php

namespace fpmoises\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Blog extends Model
{
    use HasSlug;
    protected $table = 'blogs';

    public function getSlugOptions() : SlugOptions
    {
    	return SlugOptions::create()
    	->generateSlugsFrom('title')
    	->saveSlugsTo('slug');
    }

    public function scopeName($query,$name){
      if (trim($name) == "") {

      }else
      $query->where('title',"LIKE","%$name%");
    }

    public function getCreatedAtAttribute($date){
      return new Date($date);
    }

}
