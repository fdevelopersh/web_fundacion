<?php
namespace fpmoises\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasSlug;
    protected $table = 'team';

    public function getSlugOptions() : SlugOptions
    {
    	return SlugOptions::create()
    	->generateSlugsFrom('name_team')
    	->saveSlugsTo('slug');
    }

    public function scopeName($query,$name){
        if (trim($name) == "") {
        }else
        $query->where('name_team',"LIKE","%$name%");
      }
}
