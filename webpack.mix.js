const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.sass("resources/sass/main.scss","public/css").options({processCssUrls:!1})

mix.scripts(["resources/js/jquery.min.js",
             "resources/js/jquery.viewportchecker.min.js",
             "resources/js/materialize.min.js",
             "resources/js/sss.min.js"],
             "public/js/main.js");
