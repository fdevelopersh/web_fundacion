-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-12-2019 a las 02:53:37
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fundacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `subtitle` text COLLATE utf8_bin NOT NULL,
  `image` text COLLATE utf8_bin NOT NULL,
  `image_alt` text COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `slug` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `subtitle`, `image`, `image_alt`, `content`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Formando jóvenes en Honduras', 'Conoce a David Diaz', 'david-diaz.jpg', 'david-diaz', '<p style=\"text-align: justify;\">David Diaz Tiene 21 a&ntilde;os y es parte del Proyecto Mois&eacute;s. Cuando David ten&iacute;a 7 a&ntilde;os, su madre muri&oacute; y unos a&ntilde;os m&aacute;s tarde su padre fue asesinado. David creci&oacute; en un hogar de Aldeas Infantiles SOS (una organizaci&oacute;n hondure&ntilde;a sin fines de lucro que trabaja para mantener un ambiente familiar para los ni&ntilde;os que est&aacute;n separados de sus familias de lo que el se expresa con cari&ntilde;o. David ha estado expuesto a muchas situaciones de peligros, pero Dios le ha ayudado a salir adelante haciendo lo correcto.</p>\r\n<p style=\"text-align: justify;\">Sobre el proyecto, David dice: &ldquo;He aprendido mucho. No sab&iacute;a lo que significaba trabajar el suelo o usar un machete. No sab&iacute;a c&oacute;mo trabajar con las abejas, que es lo que he hecho desde que estuve aqu&iacute; en el Proyecto. Tambi&eacute;n aprend&iacute; el proceso del caf&eacute; y aprend&iacute; acerca de Dios ... Hab&iacute;a escuchado la palabra de Dios, pero no le prest&eacute; atenci&oacute;n. Pero aqu&iacute;, en el Proyecto Mois&eacute;s, durante un seminario CREO, un hombre de Nicaragua estudi&oacute; la palabra de Dios conmigo y sent&iacute; la necesidad de ser bautizado.</p>\r\n<p style=\"text-align: justify;\">Estoy muy contento de haberme bautizado, y ha pasado un a&ntilde;o y medio desde que acept&eacute; a Cristo&rdquo;. David reflexiona sobre el dolor que sinti&oacute;, y todav&iacute;a siente, por la p&eacute;rdida de sus padres, pero afirma: \"Ahora conf&iacute;o en Dios y que &eacute;l est&aacute; guiando mi vida&rdquo;.</p>\r\n<p style=\"text-align: justify;\">Con las oportunidades que ha tenido al desarrollar las habilidades agr&iacute;colas pr&aacute;cticas que ha obtenido del Proyecto Mois&eacute;s, y su formaci&oacute;n moral y espiritual David est&aacute; listo no solo para tener m&aacute;s &eacute;xito en su vida profesional. David planea continuar sus estudios a nivel universitario. Al hablar sobre su futuro, David dice: &ldquo;Quiero estudiar y tener un plan que me ayude. Quiero graduarme de la universidad, tener una familia y siempre seguir a Dios&rdquo;.</p>', 'formando-jovenes-en-honduras', '2019-11-27 21:41:43', '2019-12-09 22:22:13'),
(3, 'Conoce parte de nuestros Jóvenes', 'Conoce a Ninrod Emil Ramos', 'ninrod-joven-proyecto-moises.jpg', 'ninrod-joven-proyecto-moises', '<p style=\"text-align: justify;\">Ninrod Emil Ramos tiene 16 a&ntilde;os edad y es de Col&oacute;n Mira Flores Cop&aacute;n. Es una persona trabajadora y le gusta cumplir con sus responsabilidades.</p>\r\n<p style=\"text-align: justify;\">Ninrod nos cuenta Su experiencia en el proyecto Mois&eacute;s, C&oacute;mo fue la primera vez que escucho del proyecto y como ha cambiado su vida con lo que ha aprendido atreves de su tiempo en el proyecto Mois&eacute;s.</p>\r\n<p style=\"text-align: justify;\">&ldquo;Me entere del proyecto por medio de mi t&iacute;a Rosa, la cual me comentaba que el proyecto era un lugar donde ayudan a j&oacute;venes para que puedan estudiar y ense&ntilde;aban valores, estudios b&iacute;blicos y que era un buen lugar. En lo laboral ayudaba a mi padre en la finca a cultivar ma&iacute;z, frijoles y cuando vine aqu&iacute; tambi&eacute;n estuve trabajando en la finca, aprend&iacute; algunas cosas tambi&eacute;n con las abejas y en peceras y tengo algo de conocimiento. En lo Espiritual. He asistido a clases de CREO y CRESCO, me ha ayudado a tener disciplina y respetar a las personas y sus opiniones.&rdquo;</p>\r\n<p style=\"text-align: justify;\">Ninrod Est&aacute; feliz de pertenecer a Proyecto Mois&eacute;s y tiene sue&ntilde;os y una visi&oacute;n el quiere graduarse del colegio, tener un buen empleo. Ayudar a su familia y su hermano peque&ntilde;o de ocho a&ntilde;os a construir un mejor futuro.</p>', 'conoce-parte-de-nuestros-jovenes', '2019-12-05 18:45:45', '2019-12-09 22:22:26'),
(4, 'Inspeccionan plantas procesadoras en Santa Rosa de Copan', 'En Occidente: DIGEPESCA inspecciona plantas procesadoras y proyectos de cultivo de tilapia en Santa Rosa de Copan.', 'inspeccion-de-plantas-procesadoras.jpg', 'inspeccion-de-plantas-procesadoras', '<p style=\"text-align: justify;\">T&eacute;cnicos de la Direcci&oacute;n General de Pesca y Acuicultura (DIGEPESCA), realizaron inspecciones y brindaron asistencia t&eacute;cnica pisc&iacute;cola en los departamentos de Lempira y Santa Rosa de Cop&aacute;n.<br /><br />La gira incluy&oacute; la visita al Proyecto Mois&eacute;s, dirigido por la fundaci&oacute;n Misi&oacute;n Hacia Arriba, (Mission Upreach Inc.) una organizaci&oacute;n no gubernamental sin fines de lucro, establecida en Santa Rosa de Cop&aacute;n, cuyo objetivo fundamental es sembrar iglesias transformadoras , por medio del equipamiento y entrenamiento a las personas, as&iacute; como servir a las comunidades en las &aacute;reas de salud, educaci&oacute;n y proyecci&oacute;n social. <br /><br />El Proyecto cultiva Tilapia roja con sistema intensivo de ciclo completo.</p>\r\n<p style=\"text-align: justify;\">La producci&oacute;n se estima en 500,000lbs/a&ntilde;o de Tilapia al a&ntilde;o, la cual es procesada, empacada y comercializada por la misma organizaci&oacute;n en los mercados locales y en San Pedro Sula.</p>\r\n<p style=\"text-align: justify;\">El personal administrativo ha hecho esfuerzos por obtener la aprobaci&oacute;n de los organismos de inocuidad alimentaria en el pa&iacute;s (ARSA, SENASA).</p>\r\n<p style=\"text-align: justify;\">Para obtener la certificaci&oacute;n correspondiente a SENASA el primer logro fue obtener DIGEPESCA.</p>\r\n<p style=\"text-align: justify;\">De agencia de regulaci&oacute;n sanitaria (ARSA) obtuvieron licencia sanitaria para el estableciendo lo que les permite poder procesar empacar y almacenar el producto llevando acabo las medidas correspondientes de higiene e inocuidad alimentaria; adem&aacute;s de esta cuentan con registro sanitario brindando seguridad a sus clientes de cada producto comercializado esperando llenar las expectativas y seguridad alimentaria para los mismos.</p>', 'inspeccionan-plantas-procesadoras-en-santa-rosa-de-copan', '2019-12-09 22:05:01', '2019-12-10 23:14:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `telephone` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `company` text COLLATE utf8_bin NOT NULL,
  `comment` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_testimonial`
--

CREATE TABLE `customers_testimonial` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `job_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `img_testimonial` text COLLATE utf8_bin NOT NULL,
  `slug` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `customers_testimonial`
--

INSERT INTO `customers_testimonial` (`id`, `name`, `job_title`, `content`, `img_testimonial`, `slug`, `created_at`, `updated_at`) VALUES
(2, 'Jhony López', 'Propietario de supermercado Mr. Market', '“Son productos de alta calidad, en el caso del pollo tiene el avaluó de SENASA y eso hace que tenga mucha credibilidad y eso hace que pueda competir tanto en el mercado de occidente como en el mercado noroccidental. Ya iniciamos vendiendo los productos y ha tenido una aceptación muy buena tanto en MR. MARKET como en la casa del pollo y esperamos seguir creciendo en volumen.”\r\n\r\nEstamos satisfechos con su excelente calidad y esperamos crezcan y avancen y siempre estamos para apoyarlos.', '5de7fbd023617-Jhony López.jpg', 'jhony-lopez', '2019-11-13 01:27:55', '2019-12-04 18:32:48'),
(3, 'Julio Molina', 'Encargado de compras - supermercado el detalle', '“Pienso que son productos de muy buena calidad para el consumidor. Esperamos que mantengan una estabilidad ofreciéndonos el producto para abastecer nuestros establecimientos y que mantengan la calidad con la que están en este momento ya que muchos empiezan con una calidad muy buena y al transcurso de los meses van perdiendo calidad. La calidad me parece excelente ya que lo hemos consumido y lo hemos tenido en prueba y nos ha dejado una muy buena impresión.', '5de7fc5c47bda-Julio Molina.jpg', 'julio-molina', '2019-11-13 01:29:25', '2019-12-04 18:35:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image_text` text COLLATE utf8_bin NOT NULL,
  `image_alt` text COLLATE utf8_bin NOT NULL,
  `image` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `images`
--

INSERT INTO `images` (`id`, `image_text`, `image_alt`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Hortalizas Productos Moisés', 'hortalizas', '5de6e8715844d-Hortalizas Productos Moisés.jpg', '2019-11-08 21:15:03', '2019-12-03 22:57:53'),
(3, 'Productos frescos', 'hortalizas-productos-frescos', '5de6e9bc5d0ea-Productos frescos.jpg', '2019-11-13 05:38:20', '2019-12-03 23:03:24'),
(4, 'Hortalizas Productos Moisés Honduras', 'productos-frescos-hortalizas', '5de6ea9b24441-Hortalizas Productos Moisés.jpg', '2019-11-27 20:12:17', '2019-12-04 00:13:13'),
(5, 'Tilapia Productos Moisés', 'tilapia-productos-moises', '5de6f021ee7b4-Tilapia Productos Moisés.jpg', '2019-12-03 23:10:05', '2019-12-03 23:30:41'),
(6, 'Hortalizas en Honduras', 'hortalizas-en-Honduras', '5de6ebff127a9-Hortalizas en Honduras.jpg', '2019-12-03 23:13:03', '2019-12-03 23:13:03'),
(7, 'Grupo de hortalizas', 'grupo-de-hortalizas', '5de6eca15cff2-Grupo de hortalizas.jpg', '2019-12-03 23:15:45', '2019-12-03 23:15:45'),
(8, 'Hortalizas cultivadas en Honduras', 'hortalizas-cultivadas-en-Honduras', '5de6ed7396584-Hortalizas cultivadas en Honduras.jpg', '2019-12-03 23:19:15', '2019-12-03 23:19:15'),
(9, 'Pollos Moisés', 'pollo-moises', '5de6ede8db754-Pollos Moisés.jpg', '2019-12-03 23:21:12', '2019-12-03 23:21:12'),
(10, 'Tilapia Roja', 'tilapia-roja', '5de6ee890a278-Tilapia Roja.jpg', '2019-12-03 23:23:53', '2019-12-03 23:23:53'),
(11, 'Cultivo de tilapia', 'cultivo-de-tilapia', '5de6f5f86e8a1-Cultivo de tilapia.jpg', '2019-12-03 23:50:42', '2019-12-03 23:55:36'),
(12, 'Cultivo de Café', 'cultivo-cafe', '5de6fb15b1640-Cultivo de Café.jpg', '2019-12-04 00:17:25', '2019-12-04 00:17:25'),
(13, 'Pollos Moisés', 'pollos-moises-productos', '5de6fbe8232ed-Pollos Moisés.jpg', '2019-12-04 00:20:56', '2019-12-04 00:20:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `img_partner` text COLLATE utf8_bin NOT NULL,
  `img_description` text COLLATE utf8_bin NOT NULL,
  `slug` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `partners`
--

INSERT INTO `partners` (`id`, `name`, `img_partner`, `img_description`, `slug`, `created_at`, `updated_at`) VALUES
(2, 'Comisariato Los Andes', '5de975acef8ac-Comisariato Los Andes.jpg', 'Logo-comisariato-los-andes', 'comisariato-los-andes', '2019-11-13 01:24:02', '2019-12-05 21:25:00'),
(3, 'Maxi Despensa', '5de9762cab333-Maxi Despensa.jpg', 'Logo-maxi-despensa', 'maxi-despensa', '2019-11-13 01:24:53', '2019-12-05 21:27:08'),
(4, 'Super Mercado el 20 menos', '5de97727039e3-Super Mercado el 20 menos.jpg', 'La Antorcha', 'super-mercado-el-20-menos', '2019-11-13 01:25:46', '2019-12-05 21:31:19'),
(5, 'Colonial', '5dcb5bb8e0b00-Colonial.jpg', 'Colonial', 'colonial', '2019-11-13 01:26:16', '2019-11-13 01:26:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `content_title` text COLLATE utf8_bin NOT NULL,
  `content_product` text COLLATE utf8_bin NOT NULL,
  `image_alt` text COLLATE utf8_bin NOT NULL,
  `image_product` text COLLATE utf8_bin NOT NULL,
  `slug` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `title`, `content_title`, `content_product`, `image_alt`, `image_product`, `slug`, `created_at`, `updated_at`) VALUES
(2, 'Hortalizas Moisés', 'Sobre Hortalizas Moisés', '<p style=\"text-align: justify;\">El &aacute;rea de hortalizas es un lugar dentro del terreno de Proyecto Mois&eacute;s que como su nombre lo dice, se encarga de la producci&oacute;n de hortalizas tanto para el consumo interno de los j&oacute;venes que viven en el proyecto como para la venta y comercializaci&oacute;n en diferentes regiones del pa&iacute;s. En la producci&oacute;n se utilizan invernaderos de estilo &ldquo;Casa maya&rdquo; donde de siembra y cultiva tomate y chile de manera intensiva. Aparte de producci&oacute;n de hortalizas que requieren menor radiaci&oacute;n diaria de sol como las brasc&aacute;ceas.</p>\r\n<p style=\"text-align: justify;\">Los estudiantes deben presentarse temprano al plantel de trabajo, donde son distribuidos por el encargado del &aacute;rea, en las diferentes tareas a desarrollar durante el d&iacute;a. Peri&oacute;dicamente, el encargado supervisa sus trabajos para corregir y ense&ntilde;arles la manera correcta de desempe&ntilde;ar las labores. Los estudiantes durante el d&iacute;a, deben formular preguntas para realizarlas al encargado de &aacute;rea o a los trabajadores. Semanalmente se les brindan charlas de conocimiento general de producci&oacute;n de hortalizas y tambi&eacute;n se les brinda charlas de campo, donde se explica alg&uacute;n problema que este afectando la producci&oacute;n en general.</p>\r\n<p style=\"text-align: justify;\"><strong>&iquest;Qu&eacute; hacemos en el &aacute;rea de hortalizas?</strong></p>\r\n<p style=\"text-align: justify;\">Desde muy temprano los trabajadores se encargan de labores como: aplicaciones de fertilizantes, monitoreos, entre otros. Las pr&aacute;cticas culturales predominan en la producci&oacute;n de hortalizas, estas nos ayudan disminuir el uso de agroqu&iacute;micos. Uno de los enfoques del &aacute;rea es el uso m&iacute;nimo de qu&iacute;micos que para comodidad del consumidor final. La producci&oacute;n de hortalizas requiere un cuidado minucioso debido a que, lo que se produce en su mayor&iacute;a son materiales vegetativos comestibles.</p>\r\n<p style=\"text-align: justify;\">&ldquo;A medida que el Proyecto Mois&eacute;s crece y se expande, tambi&eacute;n lo hacen las oportunidades disponibles para los j&oacute;venes que estudian en &eacute;l proyecto. Las cosas que aprenden no se limitan a las habilidades agr&iacute;colas; su tiempo en el Proyecto Mois&eacute;s tambi&eacute;n est&aacute; marcado por innumerables oportunidades para el desarrollo de su vida espiritual y laboral.&rdquo;</p>\r\n<p style=\"text-align: justify;\">Al consumir Productos Mois&eacute;s ayudas y formas parte de su formaci&oacute;n y desarrollo.</p>', 'hortalizas-moises', 'hortalizas-moises.jpg', 'hortalizas-moises', '2019-11-08 23:06:39', '2019-12-09 21:17:18'),
(3, 'Miel Moisés', 'Sobre Miel Moisés', '<p>&iquest;Qu&eacute; es el &aacute;rea de apicultura?</p>\r\n<p style=\"text-align: justify;\">El &aacute;rea de apicultura del Proyecto Mois&eacute;s, es un sector determinado a la educaci&oacute;n de los estudiantes, en este lugar, pueden aprender desde las t&eacute;cnicas m&aacute;s b&aacute;sicas de producci&oacute;n de abejas hasta el proceso detallado que lleva el procesamiento de la miel y las t&eacute;cnicas de an&aacute;lisis de enfermedades y plagas en las abejas. El proyecto cuenta de con 17 cajas n&uacute;cleo de 2 cuerpos compuestos de una (1) c&aacute;mara de cr&iacute;a y una (1) c&aacute;mara de miel. Ubicadas dentro de la finca de caf&eacute;.</p>\r\n<p style=\"text-align: justify;\">La producci&oacute;n de miel en el proyecto Mois&eacute;s est&aacute; destinado al procesamiento de la misma y a la venta y distribuci&oacute;n del producto a la regi&oacute;n occidental del pa&iacute;s.</p>\r\n<p style=\"text-align: justify;\">&iquest;Qu&eacute; hacemos en el &aacute;rea de apicultura?</p>\r\n<p style=\"text-align: justify;\">En el &aacute;rea de apicultura se tiene crianza de abejas africanizadas con el objetivo de ense&ntilde;anza, producci&oacute;n y comercializaci&oacute;n de productos. Los estudiantes del proyecto se encargan de todas las fases, desde el mantenimiento de los apiarios hasta la cosecha de productos y procesamiento del mismo. Los estudiantes utilizan el traje de protecci&oacute;n personal para trabajar y la primera labor del d&iacute;a es ordenar su &aacute;rea de trabajo.</p>\r\n<p style=\"text-align: justify;\">Una vez que tienen el &aacute;rea de trabajo ordenado, lo estudiantes proceden a recolectar los instrumentos que utilizar&aacute;n durante el d&iacute;a. Despu&eacute;s de definir las tareas a realizar llegan a su lugar de trabajo y la primera acci&oacute;n al entrar al apiario es la supervisi&oacute;n del estado de las mesas de sostenimiento de las cajas n&uacute;cleo y estado externo de cada una de las cajas. Estas no deben tener ni agujeros ni falsas piqueras ni otros agentes como hongos.</p>\r\n<p style=\"text-align: justify;\">Una vez que los estudiantes se realizan las labores de supervisi&oacute;n de rutina, proceden a las revisiones de las cajas n&uacute;cleo. Esta labor tiene como finalidad supervisar y determinar las plagas y enfermedades que est&aacute;n afectando la producci&oacute;n y proliferaci&oacute;n de abejas. Una vez detectada una plaga, los estudiantes deben reportarla al encargado del &aacute;rea para asignarle la practica cultural a utilizar ya que en la apicultura se evita casi a totalidad el uso de qu&iacute;micos.</p>', 'miel-moises', 'miel-moises.jpg', 'miel-moises', '2019-11-13 03:43:12', '2019-12-09 21:17:45'),
(4, 'Pollo Moisés', 'Sobre Pollo Moisés', '<p style=\"text-align: justify;\">&iquest;Qu&eacute; es el &aacute;rea de pollos?</p>\r\n<p style=\"text-align: justify;\">En pollos mois&eacute;s, nos encargamos de la producci&oacute;n de Pollos de engorde el cual es criado y engordado en las instalaciones del proyecto. Su alimentaci&oacute;n consta de prote&iacute;nas, grasas esenciales, vitaminas y minerales balanceadamente, para que al final del proceso tenga el mayor peso en un per&iacute;odo de tiempo determinado.</p>\r\n<p style=\"text-align: justify;\">Comenz&oacute; con el sue&ntilde;o de tener algunas casas de engorde y una planta de procesamiento para cultivar y preparar un alimento b&aacute;sico de la dieta hondure&ntilde;a: &ldquo;el pollo&rdquo;. Actualmente estamos produciendo en cuatro galpones de cortina abierta con capacidad para 15 mil pollos que bastecer&aacute;n gran parte de la poblaci&oacute;n a nivel regional y planeamos comenzar las operaciones en nuestra planta de procesamiento, as&iacute; como tambi&eacute;n agregar un quinto galp&oacute;n.</p>\r\n<p style=\"text-align: justify;\">El sistema de alimentaci&oacute;n es la fase m&aacute;s importante dentro del proceso del pollo, ya que constituye m&iacute;nimo el 70% del costo de producci&oacute;n y por ende es el factor primordial a considerar. Una alimentaci&oacute;n adecuada nos asegurar&aacute; en el pollo una buena constituci&oacute;n corporal en cuanto a m&uacute;sculos, huesos y grasas.</p>\r\n<p style=\"text-align: justify;\">La alimentaci&oacute;n de las aves es a partir de concentrados evitando el uso de hormonas y otros aditivos da&ntilde;inos para la salud tanto animal como humana. Nuestra agua se trata con m&eacute;todos est&aacute;ndar aceptados, vacunamos seg&uacute;n los protocolos recomendados por los veterinarios aprobados por SENASA. Buscamos constantemente mejorar y alcanzar todos los certificados de calidad posibles.</p>\r\n<p style=\"text-align: justify;\">&iquest;Qu&eacute; hacemos en el &aacute;rea de pollos?</p>\r\n<p style=\"text-align: justify;\">Cultivamos pollo de engorde est&aacute;ndar. Pollo Mois&eacute;s utiliza concentrado premium de una marca registrada y utiliza alimentadores autom&aacute;ticos y bebederos de goteo, que hacen que la comida y el agua est&eacute;n disponibles para los pollos en todo momento. El peso promedio al momento de cosecha y venta de las aves se encuentra entre 4.4 y 5.1 libras en peso vivo. Se trabaja constantemente para lograr penetrar a diferentes y nuevos mercados donde el color de la carne y el sabor sea con un est&aacute;ndar de calidad.</p>\r\n<p style=\"text-align: justify;\">Cuando dise&ntilde;amos nuestro proceso, utilizamos una combinaci&oacute;n de procesos autom&aacute;ticos y manuales para cumplir con ambos objetivos para lograr ser rentables y proteger nuestra capacidad de proporcionar empleo local. Al ser buenos vecinos, buenos ciudadanos y buenos administradores, esperamos dar el mejor ejemplo posible a los j&oacute;venes aqu&iacute; en el proyecto.</p>\r\n<p style=\"text-align: justify;\">Todos los ingresos netos generados por el negocio de pollos y productos Mois&eacute;s se utilizan para apoyar el Proyecto Mois&eacute;s y los trabajos de Misi&oacute;n Hacia Arriba en el Occidente de Honduras.</p>', 'pollo-moises', 'pollo-moises.jpg', 'pollo-moises', '2019-12-04 19:25:00', '2019-12-09 21:17:59'),
(5, 'Tilapia Moisés', 'Sobre Tilapia Moisés', '<p style=\"text-align: justify;\">El programa de tilapia es una pieza cr&iacute;tica en el plan para ayudar a Misi&oacute;n Hacia Arriba y productos Mois&eacute;s a ser sostenible. La cr&iacute;a y venta de tilapia ha sido una experiencia de aprendizaje para el Proyecto Mois&eacute;s y el dise&ntilde;o y la funci&oacute;n de la producci&oacute;n de tilapia es casi irreconocible desde hace unos a&ntilde;os. Ahora operamos cada parte de la operaci&oacute;n internamente, para que sea m&aacute;s rentable.</p>\r\n<p style=\"text-align: justify;\">&iquest;Qu&eacute; es el &aacute;rea de Tilapia Mois&eacute;s?</p>\r\n<p style=\"text-align: justify;\">Es donde desarrollamos una serie de t&eacute;cnicas y procedimientos que nos permiten criar Tilapia de la mejor calidad para satisfacer a nuestros clientes.</p>\r\n<p style=\"text-align: justify;\">Tenemos la ventaja de tener nuestra propia l&iacute;nea de tilapia porque lastimosamente la gen&eacute;tica de tilapia que existe en Honduras sufre de mucha consanguinidad. En cambio, Tilapia Mois&eacute;s su gen&eacute;tica est&aacute; en constante renovaci&oacute;n para garantizar que siempre est&eacute; funcionando en su m&aacute;ximo rendimiento.&nbsp; Tenemos nuestro propio laboratorio para asegurar desde el inicio que estamos controlando la calidad del producto desde el alev&iacute;n hasta el producto que consumes en tu mesa.&nbsp;</p>\r\n<p style=\"text-align: justify;\">&iquest;Qu&eacute; hacemos en el &aacute;rea de Tilapia?</p>\r\n<p style=\"text-align: justify;\">El proceso de calidad empieza con la cosecha de huevos a trav&eacute;s la limpieza de la boca del pez hembra.&nbsp; Los huevos luego pasan por un proceso de desinfecci&oacute;n, clasificaci&oacute;n para avalar que &uacute;nicamente los huevos f&eacute;rtiles pasan a la incubadora.&nbsp; En la incubadora los huevos pasan 8 d&iacute;as mientras se brotan y consumen el saco vitelino para poder pasarse a la selecci&oacute;n de cr&iacute;a para garantizar que la mayor&iacute;a de los peces son de g&eacute;nero masculino porque los machos se engordan m&aacute;s r&aacute;pido y esto es importante para poder alcanzar la meta de cosechas m&uacute;ltiples en un a&ntilde;o.&nbsp; Una vez que los peces han sido clasificados se pasan para la etapa de pre-engorde.&nbsp;</p>\r\n<p style=\"text-align: justify;\">La fase de pre-engorde se realiza en unos de los pocos sistemas de recirculaci&oacute;n de agua. El dise&ntilde;o de este sistema es semejante al sistema que usa la Universidad de Auburn en su granja acu&iacute;cola para sus investigaciones de sistemas de acuapon&iacute;a. Somos unos de los pocos lugares en Centro Am&eacute;rica que ha desarrollado un sistema intensivo para la fase de levantamiento de peso de los alevines.&nbsp;&nbsp;</p>\r\n<p style=\"text-align: justify;\">Los alevines son muy susceptibles a enfermedades y par&aacute;sitos en las primeras etapas de sus vidas.&nbsp; Los sistemas de recirculaci&oacute;n son adentro un invernadero.&nbsp; El dise&ntilde;o nos provee el poder de controlar el ambiente y la temperatura del agua.&nbsp;&nbsp; El poder de tener este control nos garantiza que los alevines son bien sanos y listos para la fase final.&nbsp;</p>\r\n<p style=\"text-align: justify;\">La belleza del sistema que utilizamos es el hecho que estamos conservando los recursos naturales porque el sistema reutiliza el agua a trav&eacute;s un sistema de biofiltraci&oacute;n utilizando piedra p&oacute;mez como el medio en la secci&oacute;n del biofiltro.&nbsp; Le recirculaci&oacute;n del agua nos da alto control de la calidad de agua y su temperatura asegurando condiciones &oacute;ptimas para un crecimiento alto.&nbsp; Despu&eacute;s que los peces hayan pasado por todas las fases pasan a la preparaci&oacute;n para ser entregado al consumidor final.&nbsp;</p>\r\n<p style=\"text-align: justify;\">Unas de nuestras metas es controlar toda la cadena de valor. Tenemos control desde la gen&eacute;tica que produce el alev&iacute;n hasta la cosecha de la tilapia engordada.</p>\r\n<p style=\"text-align: justify;\">Es necesario controlar los pasos de procesamiento adecuadamente es decir empacado si no se controla la cadena de valor se arriesga la calidad del producto. Precisamente por nuestro deseo de garantizar la mejor calidad posible de nuestro producto construimos nuestra propia planta de proceso.</p>', 'tilapia-roja-moises', 'tilapia-roja-moises.jpg', 'tilapia-moises', '2019-12-04 19:27:37', '2019-12-09 21:18:15'),
(6, 'Café Moisés', 'Sobre Café Moisés', '<p style=\"text-align: justify;\">El encargado de caf&eacute; trae una experiencia incre&iacute;ble en diferentes empresas, &eacute;l ha desarrollado maquinaria sofisticada que ha ayudado a mejorar empresas en sus antiguos trabajos. Un d&iacute;a su padre solicito su ayuda en el beneficio de caf&eacute; de su familia en cuanto a revisi&oacute;n, desarrollo de maquinaria y fue contratado en un puesto gerencial para tomar decisiones radicales poniendo en pr&aacute;ctica su conocimiento en maquinaria y desarrollando nuevas t&eacute;cnicas y experiencia en montaje de equipos, log&iacute;stica, desarrollo, preparaci&oacute;n y exportaci&oacute;n de caf&eacute;.</p>\r\n<p style=\"text-align: justify;\">La preparaci&oacute;n del caf&eacute; lleva consigo atenci&oacute;n delicada de parte de cada comprador y procesador quien analiza y realiza el proceso con mucha atenci&oacute;n ya que puede tener varios tipos de defectos y disminuir el costo en su valor al momento de comercializarlo. Juan en su alta experiencia lleva un control de calidad establecido para poder producir un excelente producto para nuestros consumidores.</p>\r\n<p style=\"text-align: justify;\">Hay mucho futuro en todas las &aacute;reas de proyecto Mois&eacute;s, esperamos desarrollar una preparaci&oacute;n de importaci&oacute;n y exportaci&oacute;n de caf&eacute; a trav&eacute;s de Desarrollo delicado es decir alcanzando la excelencia adquiriendo todos los sellos de calidad que se est&aacute; preparando para alcanzar un caf&eacute; 100 org&aacute;nico producidos ac&aacute; en proyecto Mois&eacute;s.</p>', 'cafe-moises-producto', 'cafe-moises-producto.jpg', 'cafe-moises', '2019-12-04 19:29:28', '2019-12-09 21:18:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `subtitle` text COLLATE utf8_bin NOT NULL,
  `img_slider` text COLLATE utf8_bin NOT NULL,
  `align_text` varchar(10) COLLATE utf8_bin NOT NULL,
  `img_alt` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `subtitle`, `img_slider`, `align_text`, `img_alt`, `created_at`, `updated_at`) VALUES
(2, 'Creando futuros, desarrollando excelencia y generando empleos', 'Proyecto Moisés', 'Productos-Moisés-generando-empleos.jpeg', 'center', 'Productos-Moisés-generando-empleos', '2019-11-13 00:57:43', '2019-12-09 21:37:13'),
(3, 'Calidad en tu mesa!', 'Solo con Productos Moisés', 'productos-moises-calidad.jpg', 'right', 'productos-moises-calidad', '2019-11-13 00:59:12', '2019-12-09 21:37:31'),
(6, 'Al consumir nuestros productos, cambias vidas', 'Productos Moisés', 'productos-moises-cambia-vidas.png', 'center', 'productos-moises-cambia-vidas', '2019-12-04 22:24:48', '2019-12-09 21:37:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subscribir`
--

CREATE TABLE `subscribir` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name_team` varchar(50) COLLATE utf8_bin NOT NULL,
  `job_title` varchar(50) COLLATE utf8_bin NOT NULL,
  `greeting_title` varchar(50) COLLATE utf8_bin NOT NULL,
  `phrase` text COLLATE utf8_bin NOT NULL,
  `image_description` text COLLATE utf8_bin NOT NULL,
  `testimonial` text COLLATE utf8_bin NOT NULL,
  `image` text COLLATE utf8_bin NOT NULL,
  `slug` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `team`
--

INSERT INTO `team` (`id`, `name_team`, `job_title`, `greeting_title`, `phrase`, `image_description`, `testimonial`, `image`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Ing. Daniel Viera', 'Encargado de huerto y miel', '“Mejora Continua e Innovación “', 'Apoyar a personas en riesgo social y aconsejarlos con valores espirituales.', 'encargado-huerto-miel-daniel-viera', '<p style=\"text-align: justify;\">&ldquo;Mi nombre es Daniel Viera procedente de San Pedro Sula, Honduras. Soy graduado de la carrera de Agroindustria alimentaria en la Escuela Agr&iacute;cola Panamericana. Durante mi carrera profesional he trabajado en diferentes &aacute;reas de la producci&oacute;n agr&iacute;cola que me han brindado la formaci&oacute;n necesaria para lograr obtener resultados &oacute;ptimos apoy&aacute;ndome en el dicho de &ldquo;Mejora Continua&rdquo; e &ldquo;Innovaci&oacute;n para Mejorar&rdquo;, los cuales han ayudado a implementar procesos utilizando la m&iacute;nima cantidad de recursos para optimizar los resultados.</p>\r\n<p style=\"text-align: justify;\">Desde mi juventud trabaje con j&oacute;venes en la iglesia y otros lugares de labor social como casas hogares (Orfanatos). Nac&iacute; y crec&iacute; en un hogar cristiano lleno de valores tanto morales como espirituales. Cada ma&ntilde;ana, mis padres me ense&ntilde;aron orar y agradecer a Dios por regalarme un nuevo d&iacute;a, ense&ntilde;anza que hasta el d&iacute;a de hoy siguen cincelada en mi coraz&oacute;n. Durante mi juventud mis padres se encargaron de guiarme y aconsejarme hacia el camino correcto permiti&eacute;ndome tomar mis propias decisiones, pero enderezando mi caminar cuando esto fuera necesario.</p>\r\n<p style=\"text-align: justify;\">Observando mis acciones, logr&eacute; entender la importancia e impacto social que los j&oacute;venes tienen en la sociedad y la importancia de dirigirlos para cambiar la cultura de la misma. Cuando entend&iacute; esta importancia, decid&iacute; empezar a actuar en favor de la sociedad apoyando personas en riesgo social y aconsejando con los valores espirituales y morales que me fueron inculcados. No todo fue bueno, hubo altos y bajos dentro de este proceso, sin embargo, entend&iacute; que cada persona tiene un prop&oacute;sito y destino en Dios el cual debe de cumplir y nosotros, como hijos de Dios, debemos ayudar a cumplir. &ldquo;</p>', 'encargado-huerto-miel-daniel-viera.jpg', 'ing-daniel-viera', '2019-11-13 02:29:10', '2019-12-09 20:03:23'),
(2, 'Ing. Michael Bridges', 'Encargado de Pollos', '“Esforzandome por hacer una diferencia“', 'Tener un proyecto autosuficiente y ayudar a jóvenes a convertirse en líderes.', 'encargado-pollos-michael-bridges', '<p style=\"text-align: justify;\">Durante 22 a&ntilde;os, Michael Bridges (Jefe de Operaciones Av&iacute;colas) trabaj&oacute; como ingeniero de procesos para Michelin Tire Corp. y pas&oacute; un tiempo considerable entrenando ingenieros tanto en Polonia como en India. Michael lleg&oacute; a Honduras en 2016 en una decisi&oacute;n que le da cr&eacute;dito a Dios.</p>\r\n<p style=\"text-align: justify;\">En su tiempo con Misi&oacute;n Hacia Arriba, Michael ha disfrutado el proceso de llegar a las personas. &Eacute;l dice: \"Es genial poder luchar junto a Jes&uacute;s para lograr un cambio en la vida de j&oacute;venes y viejos por igual y verlo tomar mi debilidad y mis incapacidades y verlo trabajar poderosamente.</p>\r\n<p style=\"text-align: justify;\">Aqu&iacute; en el Proyecto Mois&eacute;s, nos esforzamos por hacer una diferencia para Cristo, en el Occidente de Honduras. Nuestro objetivo es tener un proyecto autosuficiente que ayude a los hombres j&oacute;venes a convertirse en l&iacute;deres que puedan afectar la vida de sus familias y comunidades. Una parte de ayudar a estos j&oacute;venes a convertirse en algo diferente, es mostrarles una buena administraci&oacute;n en lo que hacemos. En Pollo Mois&eacute;s, nos esforzamos por hacer nuestra parte.</p>\r\n<p style=\"text-align: justify;\">Estos j&oacute;venes ser&aacute;n pilares en sus comunidades. Muchos de ellos no hab&iacute;an recibido una educaci&oacute;n espiritual y hab&iacute;an recibido muy poca educaci&oacute;n de valores en sus casas, y con la ayuda de las ense&ntilde;anzas que diariamente se les brindan todos se han convertido en cristianos, con la idea de que volver&aacute;n a sus comunidades para comenzar algunos negocios, proporcionar empleo con liderazgo y lo mejor de todo, llevar&aacute;n a Cristo a esos pueblos.</p>\r\n<p style=\"text-align: justify;\">El Proyecto con los pollos est&aacute; preparado para proporcionar una fuente sustancial de ingresos regulares a Misi&oacute;n Hacia Arriba y productos Mois&eacute;s. Ingresos con los cuales se podr&aacute; financiar m&aacute;s j&oacute;venes y ni&ntilde;os para que puedan estudiar y apoyar con la formaci&oacute;n de nuevas iglesias en diferentes comunidades de occidente donde estas puedan llevar a nuestro se&ntilde;or Jesucristo a los corazones de muchas m&aacute;s personas. So&ntilde;amos con que el proyecto y productos Mois&eacute;s pueda expandirse continuamente y as&iacute; aportar un poco m&aacute;s a la siembra de semillas humanas en tierras f&eacute;rtiles que son las personas de las comunidades.</p>', 'encargado-pollos-michael-bridges.jpg', 'ing-michael-bridges', '2019-11-13 02:30:13', '2019-12-09 20:04:02'),
(3, 'Ing. Juan José Urquía', 'Encargado de café', '“Esperamos alcanzar un lugar de sostenibilidad“', 'Ayudar a hombres jóvenes con recursos financieros limitados.', 'encargado-de-cafe-juan-jose', '<p style=\"text-align: justify;\">Juan Jos&eacute; Urqu&iacute;a vino a trabajar con Mission UpReach como jefe de producci&oacute;n de caf&eacute;. Juan trae consigo 18 a&ntilde;os de experiencia en caf&eacute; que ha ayudado a elevar el nivel de producci&oacute;n de caf&eacute; en cantidad.</p>\r\n<p style=\"text-align: justify;\">El proyecto Mois&eacute;s es la forma en que \"ayuda a los hombres j&oacute;venes con recursos financieros limitados. les proporciona sus necesidades b&aacute;sicas y les ayuda en su formaci&oacute;n profesional y cristiana; convirti&eacute;ndolos en l&iacute;deres que ayudar&aacute;n a transformar sus comunidades y difundir el evangelio&rdquo;.</p>\r\n<p style=\"text-align: justify;\">Juan Jos&eacute; tambi&eacute;n ve el impacto positivo que el Proyecto Mois&eacute;s est&aacute; teniendo en la econom&iacute;a local. &Eacute;l atribuye esto a todos los trabajos que se han creado y a la cantidad de bienes comprados por el Proyecto Mois&eacute;s, que beneficia a las empresas locales.</p>\r\n<p style=\"text-align: justify;\">el caf&eacute; ha sido uno de los productos m&aacute;s grandes de Honduras con la preparaci&oacute;n y comercializaci&oacute;n adecuadas, podremos vender mucho caf&eacute; y proporcionar fondos que respalden la sostenibilidad de cultivar y tambi&eacute;n proporcionar recursos para Mission UpReach</p>\r\n<p style=\"text-align: justify;\">Como disc&iacute;pulos de Jes&uacute;s, esas mismas ideas y objetivos deber&iacute;an ser evidentes en nuestros ministerios. lo que Dios nos ha dado significa encontrar personas excelentes cuya visi&oacute;n y experiencia permitan que esos programas prosperen.</p>\r\n<p style=\"text-align: justify;\">El incre&iacute;ble conocimiento de Juan sobre el caf&eacute; y su pasi&oacute;n por perfeccionar el arte de cultivarlo lo convierten en un activo valioso para Mission UpReach.</p>\r\n<p style=\"text-align: justify;\">mediante su consumo estamos ayudando a los peque&ntilde;os agricultores en su producci&oacute;n de caf&eacute; y no solo proporcionando ingresos y empleo, sino que tambi&eacute;n siendo una herramienta para la evangelizaci&oacute;n ya que el proyecto Mois&eacute;s es su objetivo principal ayudar en la vida espiritual de cada joven. Esperamos alcanzar un lugar de sostenibilidad que nos permita explorar nuestros nuevos sue&ntilde;os sobre c&oacute;mo apoyar y alimentar a la comunidad y difundir el evangelio.</p>', 'encargado-de-cafe-juan-jose.jpg', 'ing-juan-jose-urquia', '2019-11-13 02:31:42', '2019-12-09 20:04:19'),
(4, 'Ing. Jon Stacy', 'Encargado de Tilapia', '“Formar hombres preparados y capaces.”', 'Es maravilloso ver los jóvenes crecer durante sus años en el programa.', 'encargado-tilapia-jon', '<p style=\"text-align: justify;\">Jon Stacy creci&oacute; en una familia entregada a Dios y a la familia de Dios, la iglesia. En el a&ntilde;o 2011, Jon, junto con su esposa y sus 3 hijos, vinieron a vivir en Santa Rosa de Cop&aacute;n para trabajar con Misi&oacute;n Hacia Arriba. A trav&eacute;s de estos 8 a&ntilde;os, Jon ha aprendido hablar idioma espa&ntilde;ol, y ha servido en muchas distintas funciones con la misi&oacute;n. Con una licenciatura en administraci&oacute;n de empresas y en contabilidad, Jon trae su gran experiencia en manejo de negocios y contadur&iacute;a. Aparte de su trabajo con la Misi&oacute;n, Jon sirve como di&aacute;cono en la Iglesia de Cristo en Barrio Bel&eacute;n, SRC, y es el l&iacute;der del Ministerio de J&oacute;venes.</p>\r\n<p style=\"text-align: justify;\">Siempre ha tenido en su coraz&oacute;n una pasi&oacute;n por los j&oacute;venes y por trabajar en la misi&oacute;n que Dios tiene para su vida.</p>\r\n<p style=\"text-align: justify;\">En el a&ntilde;o 2015 cuando adquirimos el Proyecto Mois&eacute;s, Jon empez&oacute; a trabajar con el sue&ntilde;o de tener una producci&oacute;n de tilapia para beneficiar al Proyecto Mois&eacute;s, junto a los proyectos de la Misi&oacute;n Hacia Arriba. Aunque no ten&iacute;a entrenamiento espec&iacute;fico en asuntos de tilapia, Dios prove&iacute;a varios hermanos de la iglesia y compa&ntilde;eros de la Universidad Auburn en Alabama, USA, quien nos han ayudado por medio de compartir con nosotros su profundo conocimiento de c&oacute;mo desarrollar un h&aacute;bitat ideal para la tilapia. Tambi&eacute;n nos ayudaron a desarrollar el sistema IPRS. Este sistema nos ayud&oacute; aumentar la producci&oacute;n por 500%.</p>\r\n<p style=\"text-align: justify;\">Como director del Proyecto Mois&eacute;s, Jon se encarga no solamente de la producci&oacute;n de tilapia, sino tambi&eacute;n de las ventas de todos los productos Mois&eacute;s, las actividades administrativas, y todos los asuntos de los j&oacute;venes que viven y estudian en el proyecto.</p>\r\n<p style=\"text-align: justify;\">Jon dice que el mejor sentido es ver estos j&oacute;venes quienes entran el proyecto sin esperanza y seg&uacute;n los j&oacute;venes, sin un futuro, crecer durante sus a&ntilde;os en el programa y salir hombres preparados y capaces de enfrentar las diversas pruebas de la vida y con una visi&oacute;n clara para usar lo aprendido para el beneficio de sus familias y sus comunidades. &Eacute;l dice: - &ldquo;es como sentarse en primera fila para ver las obras de Dios en las vidas de los j&oacute;venes&rdquo;.</p>', 'encargado-tilapia-jon.jpg', 'ing-jon-stacy', '2019-12-05 23:31:35', '2019-12-09 20:04:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_user` int(11) NOT NULL,
  `image_user` text COLLATE utf8_unicode_ci NOT NULL,
  `image_background` text COLLATE utf8_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `type_user`, `image_user`, `image_background`, `api_token`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Admin', 1, '5de578e7adced-Admin.png', '5de578e7aee65-background-Admin.jpg', NULL, 'admin@moises.com', NULL, '$2y$10$PkdXG.TpCB4OoruIDqkGveaLZY7LSAULhTzLxNCw53/vZGjbQzhTS', NULL, '2019-11-14 01:20:11', '2019-12-02 20:49:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `name_video` varchar(50) COLLATE utf8_bin NOT NULL,
  `link_video` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `name_video`, `link_video`, `created_at`, `updated_at`) VALUES
(2, 'Tilapia', 'https://www.youtube.com/watch?v=o18J4G6VgUA', '2019-11-08 21:37:01', '2019-12-03 20:44:07'),
(5, 'Café', 'https://www.youtube.com/watch?v=voqZ7SM-cG4', '2019-11-13 06:34:29', '2019-12-03 20:45:09'),
(6, 'Miel', 'https://www.youtube.com/watch?v=qLo6VWC6nfQ', '2019-12-03 20:46:59', '2019-12-03 20:46:59'),
(7, 'Hortalizas', 'https://www.youtube.com/watch?v=rOGiNsV4Yyw', '2019-12-03 20:48:32', '2019-12-03 20:48:32');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `customers_testimonial`
--
ALTER TABLE `customers_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subscribir`
--
ALTER TABLE `subscribir`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers_testimonial`
--
ALTER TABLE `customers_testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `subscribir`
--
ALTER TABLE `subscribir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
