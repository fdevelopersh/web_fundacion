@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Crear Noticia</h3>
         <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/blog/store') }}" method="POST" class="col s12">
           @csrf
           @if ($errors->all())
           <div class="alert alert-danger">
             <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
             </ul>
           </div>
           @endif
           <div class="row">
             <div class="center-align">
               <img class="img-avatar-team-admin" src="/img/default.jpg" alt="">
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="title" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Título de noticia</label>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="subtitle" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Sub-título noticia</label>
             </div>
             <div class="input-field col s12 m12 l12">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="image_alt" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Descripción de imagen(alt)</label>
             </div>
             <div class="input-field col s12 m12 l12">
               <p>Contenido de Noticia</p>
               <textarea name="editor_content" id="myEditor"></textarea>
             </div>
             <div class="file-field input-field" style="clear:both;">
               <div class="input-field col s12 m6 l6 offset-l3">
                 <div class="btn blue-edit">
                   <span>Buscar imagen</span>
                   <input name="image_blog" id="fileuser" type="file">
                 </div>
                 <div class="file-path-wrapper">
                   <input class="file-path validate" type="text">
                 </div>
               </div>
             </div>
           </div>
           <div class="center-align">
             <button class="btn green-save waves-effect waves-light" type="submit" name="action">Guardar
               <i class="material-icons right">save</i>
             </button>
             <a href="/fwdfundacion/admin-panel/blog"
              class="btn waves-effect blue-edit waves-light" type="submit" name="action">Atrás
              <i class="material-icons right">arrow_back</i>
             </a>
           </div>
         </form>
    </div>
  </div>
  <script>
  var editor_config = {
    path_absolute : "/",
    selector: "#myEditor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
  </script>
@endsection
