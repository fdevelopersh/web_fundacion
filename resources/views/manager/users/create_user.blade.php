@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Crear Usuario</h3>
         <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/users/store') }}" method="POST" class="col s12">
          @csrf
          <div class="row">
             <div class="center-align">
               <img class="img-avatar circle" src="/img/users/default.png" alt="">
             </div>
             @if ($errors->all())
             <div class="alert alert-danger">
               <ul>
                 @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
                 @endforeach
               </ul>
             </div>
             @endif
             <div class="file-field input-field">
               <div class="input-field col s12 m6 offset-m3 l6 offset-l3">
                 <div class="btn blue-edit">
                   <span>Buscar imagen</span>
                   <input name="image_user" type="file">
                 </div>
                 <div class="file-path-wrapper">
                   <input class="file-path validate" type="text">
                 </div>
               </div>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea id="icon_prefix2" name="name" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Nombre de Usuario</label>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea id="icon_prefix2" name="email" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Correo electronico</label>
             </div>
             <div class="col s12 m6 l6">
                <label>Tipo de Usuario</label>
                 <select name="type_user" class="browser-default">
                   <option value="1">Administrador</option>
                   <option value="2">Usuario</option>
                 </select>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea id="icon_prefix2" name="password" class="materialize-textarea textarea2" data-length="120"></textarea>
               <label for="icon_prefix2">Contraseña</label>
             </div>
             <div class="file-field input-field">
               <div class="input-field col s12 m6 offset-m3 l6 offset-l3">
                 <div class="btn blue-edit">
                   <span>Imagen de portada</span>
                   <input name="image_background" type="file">
                 </div>
                 <div class="file-path-wrapper">
                   <input class="file-path validate" type="text">
                 </div>
               </div>
             </div>
           </div>
           <div class="center-align">
             <button class="btn green-save waves-effect waves-light" type="submit" name="action">Guardar
               <i class="material-icons right">save</i>
             </button>
             <a href="/fwdfundacion/admin-panel/users"
              class="btn waves-effect blue-edit waves-light" type="submit" name="action">Atrás
              <i class="material-icons right">arrow_back</i>
            </a>
           </div>
         </form>
    </div>
  </div>
@endsection
