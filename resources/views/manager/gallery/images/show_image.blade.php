@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Galería de imágenes</h3>
      <div class="col s12 m3 l3 center-align">
        <a href="/fwdfundacion/admin-panel/galeria/imagenes/create" class="buttonp"><i class="material-icons right">add</i>Aregar</a>
      </div>
      <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/galeria/imagenes') }}" method="get" role="form">
        @csrf
        <div class="col s12 m4 l3 center-align">
          <a href="/fwdfundacion/admin-panel/galeria/imagenes/" class="buttonp"><i class="material-icons left">autorenew</i>Actualizar</a>
        </div>
        <div class="col s12 m2 l2 center-align">
          <button class="buttonp" type="submit">
            <i class="material-icons">search</i></button>
        </div>
        <div class="col s12 m3 center-align no-padding">
         <input name="name" type="text" placeholder="Buscar" >
        </div>
      </form>
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Texto pie de Imagen</th>
            <th>Descripción de Imagen(alt)</th>
            <th>Imagen</th>
            <th>Editar</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($images as $key => $image)
          <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $image->image_text }}</td>
            <td>{{ $image->image_alt }}</td>
            <td><img class="size-img-table-panel" src="/img/gallery/images/{{ $image->image }}" alt="{{ $image->image_alt }}"></td>
            <td>
              <a href="/fwdfundacion/admin-panel/galeria/imagenes/edit/{{ $image->id }}"
                class="waves-effect blue-edit darken-1 btn-small">
                <i class="material-icons">colorize</i></a></td>
                  <td>
                <a onClick="return confirm('¿Estás seguro que deseas eliminar esta publicación?');"
                href="/fwdfundacion/admin-panel/galeria/imagenes/destroy/{{ $image->id }}" class="waves-effect red-delete btn-small"><i class="material-icons">delete</i></a>
            </td>
          </tr>
              @endforeach
        </tbody>
      </table>
    </div>
    <a href="/fwdfundacion/admin-panel"
        class="buttonp right" type="submit" name="action">Atrás
       <i class="material-icons right">arrow_back</i>
    </a>
  </div>
@endsection
