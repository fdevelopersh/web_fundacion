@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Editar Imagen</h3>
         <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/galeria/imagenes/update/'.$image->id) }}" method="POST" class="col s12">
          @csrf
          <div class="row">
             <div class="center-align">
               <img class="img-avatar-team-admin" src="/img/gallery/images/{{ $image->image }}" alt="">
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea id="icon_prefix2" name="image_text" class="materialize-textarea">{{ $image->image_text }}</textarea>
               <label for="icon_prefix2">Texto pie de imagen</label>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea id="icon_prefix2" name="image_alt" class="materialize-textarea">{{ $image->image_alt }}</textarea>
               <label for="icon_prefix2">Descripción de imagen(alt)</label>
             </div>
             <div class="file-field input-field">
               <div class="input-field col s12 m6 l6 offset-l3">
                 <div class="btn blue-edit">
                   <span>Cambiar imagen</span>
                   <input name="image" id="fileuser" type="file">
                 </div>
                 <div class="file-path-wrapper">
                   <input class="file-path validate" type="text">
                 </div>
               </div>
             </div>
           </div>
           <div class="center-align">
             <button class="btn green-save waves-effect waves-light" type="submit" name="action">Guardar
               <i class="material-icons right">save</i>
             </button>
             <a href="/fwdfundacion/admin-panel/galeria/imagenes"
              class="btn waves-effect blue-edit waves-light" type="submit" name="action">Atrás
              <i class="material-icons right">arrow_back</i>
            </a>
           </div>
         </form>
    </div>
  </div>
@endsection
