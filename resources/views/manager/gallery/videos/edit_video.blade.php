@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Editar Video</h3>
         <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/galeria/videos/update/'.$video->id) }}" method="POST" class="col s12">
          @csrf
          <div class="row">
              <div class="input-field col s12 m12 l12">
                  <i class="material-icons prefix">mode_edit</i>
                  <textarea id="icon_prefix2" name="name_video" class="materialize-textarea">{{ $video->name_video }}</textarea>
                  <label for="icon_prefix2">Nombre del Video</label>
                </div>
             <div class="input-field col s12 m12 l12">
               <i class="material-icons prefix">mode_edit</i>
               <textarea id="icon_prefix2" name="link_video" class="materialize-textarea">{{ $video->link_video }}</textarea>
               <label for="icon_prefix2">Link Video</label>
             </div>
           </div>
           <div class="center-align">
             <button class="btn green-save waves-effect waves-light" type="submit" name="action">Guardar
               <i class="material-icons right">save</i>
             </button>
             <a href="/fwdfundacion/admin-panel/galeria/videos"
              class="btn waves-effect blue-edit waves-light" type="submit" name="action">Atrás
              <i class="material-icons right">arrow_back</i>
            </a>
           </div>
         </form>
    </div>
  </div>
@endsection
