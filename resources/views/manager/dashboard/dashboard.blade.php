@extends('layouts.admin-manager')
@section('content')
    <a class="weatherwidget-io" href="https://forecast7.com/es/14d85n89d15/copan-ruinas/" data-label_1="COPÁN" data-label_2="WEATHER" data-theme="pillows" >COPÁN WEATHER</a>
    <script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
    </script>
    <div id="content-calendar-admin">
      <div class="row center-align">
        <div class="col s12 m12 l12">
          <iframe id="calendar-size" src="https://calendar.google.com/calendar/embed?height=450&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FTegucigalpa&amp;src=ZXMuaG4jaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%230B8043&amp;showPrint=0&amp;showTabs=1&amp;showNav=1" style="border-width:0" width="800" height="450" frameborder="0" scrolling="no"></iframe>
        </div>
      </div>
    </div>
@endsection
