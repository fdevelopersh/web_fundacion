@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Productos</h3>
      <div class="col s12 m3 l3 center-align">
        <a href="/fwdfundacion/admin-panel/products/create" class="buttonp"><i class="material-icons right">add</i>Agregar</a>
      </div>
      <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/products/') }}" method="get" role="form">
       @csrf
        <div class="col s12 m4 l3 center-align">
          <a href="/fwdfundacion/admin-panel/products" class="buttonp"><i class="material-icons left">autorenew</i>Actualizar</a>
        </div>
        <div class="col s12 m2 l2 center-align">
          <button class="buttonp" type="submit">
            <i class="material-icons">search</i></button>
        </div>
        <div class="col s12 m3 center-align no-padding">
         <input name="name" type="text" placeholder="Buscar producto" >
        </div>
      </form>
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Título Producto</th>
            <th>Título del contenido</th>
            <th>Imagen del Producto</th>
            <th>Editar</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($products as $key => $product)
              <tr>
              <td>{{ $key+1 }}</td>
              <td>{{ $product->title }}</td>
              <td>{{ $product->content_title }}</td>
              <td><img class="size-img-table-panel" src="/img/products/{{ $product->image_product }}" alt="miel-abeja"></td>
              <td>
                <a href="/fwdfundacion/admin-panel/products/edit/{{ $product->id }}"
                  class="waves-effect blue-edit darken-1 btn-small">
                  <i class="material-icons">colorize</i></a></td>
                    <td>
                  <a onClick="return confirm('¿Estás seguro que deseas eliminar esta publicación?');"
                  href="/fwdfundacion/admin-panel/products/destroy/{{ $product->id }}" class="waves-effect red-delete btn-small"><i class="material-icons">delete</i></a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <a href="/fwdfundacion/admin-panel/"
        class="buttonp right" type="submit" name="action">Atrás
       <i class="material-icons right">arrow_back</i>
    </a>
  </div>
@endsection
