@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Slider</h3>
      <div class="col s12 m3 l3 center-align">
        <a href="/fwdfundacion/admin-panel/sliders/create" class="buttonp"><i class="material-icons right">add</i>Agregar</a>
      </div>
      <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/slider') }}" method="get" role="form">
        <div class="col s12 m4 l3 center-align">
          <a href="/fwdfundacion/admin-panel/slider" class="buttonp"><i class="material-icons left">autorenew</i>Actualizar</a>
        </div>
        <div class="col s12 m2 l2 center-align">
          <button class="buttonp" type="submit">
            <i class="material-icons">search</i></button>
        </div>
        <div class="col s12 m3 l3 center-align no-padding">
         <input name="name" type="text" placeholder="Buscar" >
        </div>
      </form>
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Título Slider</th>
            <th>Imagen Slider</th>
            <th>Editar</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($sliders as $key => $slider)
          <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $slider->title }}</td>
            <td> <img class="size-img-table-panel" src="/img/slider/{{$slider->img_slider}}" alt="{{$slider->img_alt}}"> </td>
            <td>
              <a href="/fwdfundacion/admin-panel/sliders/edit/{{ $slider->id }}"
                class="waves-effect blue-edit darken-1 btn-small">
                <i class="material-icons">colorize</i></a></td>
                  <td>
                <a onClick="return confirm('¿Estás seguro que deseas eliminar esta publicación?');"
                href="/fwdfundacion/admin-panel/sliders/destroy/{{ $slider->id }}" class="waves-effect red-delete btn-small"><i class="material-icons">delete</i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <a href="/fwdfundacion/admin-panel/"
        class="buttonp right" type="submit" name="action">Atrás
       <i class="material-icons right">arrow_back</i>
    </a>
  </div>
@endsection
