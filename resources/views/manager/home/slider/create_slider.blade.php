@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Crear Slider</h3>
         <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/sliders/store') }}" method="POST" class="col s12">
           @csrf
           @if ($errors->all())
           <div class="alert alert-danger">
             <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
             </ul>
           </div>
           @endif
           <div class="row">
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="title" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Título</label>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="subtitle" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Sub-título</label>
             </div>
             <div class="col s12 m6 l6">
                <label>Posición de texto en el slider</label>
                 <select name="align_text" class="browser-default">
                   <option value="center">Centro</option>
                   <option value="right">Derecha</option>
                   <option value="left">Izquierda</option>
                 </select>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="img_alt" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Descripción de imagen(alt)</label>
             </div>
             <div class="file-field input-field">
               <div class="input-field col s12 m6 l6">
                 <div class="btn blue-edit">
                   <span>Buscar imagen</span>
                   <input name="image_slider" id="fileslider" type="file">
                 </div>
                 <div class="file-path-wrapper">
                   <input class="file-path validate" type="text">
                 </div>
               </div>
             </div>
           </div>
           <div class="center-align">
             <button class="btn green-save waves-effect waves-light" type="submit" name="action">Guardar
               <i class="material-icons right">save</i>
             </button>
             <a href="/fwdfundacion/admin-panel/slider"
              class="btn waves-effect blue-edit waves-light" type="submit" name="action">Atrás
              <i class="material-icons right">arrow_back</i>
            </a>
           </div>
         </form>
    </div>
  </div>
@endsection
