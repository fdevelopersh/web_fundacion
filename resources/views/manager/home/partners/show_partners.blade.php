@extends('layouts.admin-manager')
@section('content')
<div class="content-wrapper-panel">
  <div class="row">
    <h3 class="center-align margin-title-panel-content title-parallax-view">Empresas Afiliadas</h3>
    <div class="col s12 m3 l4 center-align">
      <a href="/fwdfundacion/admin-panel/patrocinadores/create" class="buttonp"><i class="material-icons right">add</i>Agregar</a>
    </div>
    <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/patrocinadores') }}" method="get" role="form">
      <div class="col s12 m4 l3 center-align">
        <a href="/fwdfundacion/admin-panel/patrocinadores" class="buttonp"><i class="material-icons left">autorenew</i>Actualizar</a>
      </div>
      <div class="col s12 m2 l2 center-align no-padding">
        <button class="buttonp" type="submit">
          <i class="material-icons">search</i></button>
      </div>
      <div class="col s12 m3 l3 center-align no-padding">
       <input name="name" type="text" placeholder="Buscar" >
      </div>
    </form>
    <table>
      <thead>
        <tr>
          <th>#</th>
          <th>Título de imagen</th>
          <th>Descripción de imagen</th>
          <th>Imagen</th>
          <th>Editar</th>
          <th>Eliminar</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($partners as $key => $partner)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{  $partner->name }}</td>
          <td>{{ $partner->img_description }}</td>
          <td><img class="size-img-table-panel-cust" src="/img/partners/{{ $partner->img_partner }}" alt="{{ $partner->img_description }}"></td>
          <td>
            <a href="/fwdfundacion/admin-panel/patrocinadores/edit/{{ $partner->id }}"
              class="waves-effect blue-edit darken-1 btn-small">
              <i class="material-icons">colorize</i></a></td>
                <td>
              <a onClick="return confirm('¿Estás seguro que deseas eliminar esta publicación?');"
              href="/fwdfundacion/admin-panel/patrocinadores/destroy/{{ $partner->id }}" class="waves-effect red-delete btn-small"><i class="material-icons">delete</i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <a href="/fwdfundacion/admin-panel/"
      class="buttonp right" type="submit" name="action">Back
     <i class="material-icons right">arrow_back</i>
  </a>
</div>
@endsection
