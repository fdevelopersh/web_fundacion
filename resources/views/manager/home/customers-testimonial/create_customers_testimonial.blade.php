@extends('layouts.admin-manager')
@section('content')
  <div class="content-wrapper-panel">
    <div class="row">
      <h3 class="center-align margin-title-panel-content title-parallax-view">Crear Testimonio</h3>
         <form enctype="multipart/form-data" action="{{ URL('/fwdfundacion/admin-panel/clientes-testimonios/store') }}" method="POST" class="col s12">
           @csrf
           @if ($errors->all())
           <div class="alert alert-danger">
             <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
             </ul>
           </div>
           @endif
           <div class="row">
             <div class="center-align">
               <img class="img-avatar circle" src="./img/customers-testimonial/default.png" alt="">
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="name" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Nombre y Apellido</label>
             </div>
             <div class="input-field col s12 m6 l6">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="job_title" id="icon_prefix2" class="materialize-textarea"></textarea>
               <label for="icon_prefix2">Cargo</label>
             </div>
             <div class="input-field col s12 m12 l12">
               <i class="material-icons prefix">mode_edit</i>
               <textarea name="content" id="icon_prefix2" class="materialize-textarea textarea2" data-length="480"></textarea>
               <label for="icon_prefix2">Contenido</label>
             </div>
             <div class="file-field input-field">
               <div class="input-field col s12 m6 l6 offset-l3">
                 <div class="btn blue-edit">
                   <span>Buscar imagen</span>
                   <input name="image_testimonial" id="fileuser" type="file">
                 </div>
                 <div class="file-path-wrapper">
                   <input class="file-path validate" type="text">
                 </div>
               </div>
             </div>
           </div>
           <div class="center-align">
             <button class="btn green-save waves-effect waves-light" type="submit" name="action">Guardar
               <i class="material-icons right">save</i>
             </button>
             <a href="/fwdfundacion/admin-panel/clientes-testimonios"
              class="btn waves-effect blue-edit waves-light" type="submit" name="action">Atrás
              <i class="material-icons right">arrow_back</i>
            </a>
           </div>
         </form>
    </div>
  </div>
@endsection
