@extends('layouts.global_main')
@section('title', 'Contáctanos - Productos Moisés')
@section('urlcanonical','https://productosmoises.com/contactanos')
@section('description', 'Escribenos sin ningún compromiso')
@section('keywords', 'contacto, productos moisés')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Contáctanos | Productos Moisés')
@section('ogurl', 'https://productosmoises.com/contactanos')
@section('ogimage', 'https://productosmoises.com/img/productos-moises-contactanos.jpg')
@section('ogdescription', 'Escribenos sin ningún compromiso')
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Contáctanos</h1>
        <div class="parallax"><img src="./img/productos-moises-contactanos.jpg" alt="productos-moises-contactanos"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
    <div class="content-wrapper">
        <div class="container">
          <div class="padding-top-buttom">
            <h3 class="center-align title no-margin">Contáctanos</h3>
            <h5 class="center-align no-margin sub-title">Escribenos sin ningún compromiso</h5>
          </div>
          @foreach ($errors->all() as $error)
            <li class="alert-contact-us">{{ $error }}</li>
          @endforeach
          <div class="row">
            <form action="{{ URL('/contactanos/store') }}" method="post" enctype="multipart/form-data" class="col s12">
              @csrf
              {!! RecaptchaV3::initJs() !!}
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">account_circle</i>
                  <input name="name" id="name" type="text" class="validate">
                  <label for="name">Nombre</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">phone</i>
                  <input name="telephone" id="telephone" type="tel" class="validate">
                  <label for="telephone">Telefono</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">email</i>
                  <input name="email" id="email" type="email" class="validate">
                  <label for="email">Email</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">business</i>
                  <input name="company" id="company" type="text" class="validate">
                  <label for="company">Empresa</label>
                </div>
                <div class="input-field col s12 m12 l12">
                  <i class="material-icons prefix">create</i>
                  <textarea name="comment" id="textarea1" class="materialize-textarea" data-length="120"></textarea>
                  <label for="textarea1">Comentario</label>
                </div>
              </div>
              <div class="center-align">
                <button class="button button1" type="submit" name="action">Enviar Ahora
                <i class="material-icons right">send</i>
                </button>
              </div>
              {!! RecaptchaV3::field('contacto') !!}
            </form>
          </div>
        </div>
    </div>
  </section>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  @if ($message = Session::get('success'))
    <script type="text/javascript">
       Swal.fire({
          icon: 'success',
          title:'Success!',
          html:"<p>{{ $message }}</p>"
       })
    </script>
  @endif
@endsection
