@extends('layouts.app')

@section('content')
  <form class="form-signin" method="POST" action="{{ route('login') }}">
    <img src="/img/logo-productos-moises.png" alt="">
    <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesión</h1>
      @csrf
      <div class="form-group row">
          <div class="col-md-12">
              <input id="email" type="email" placeholder="{{ __('Correo electrónico') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row mb-0">
          <div class="col-md-12">
              <input id="password" type="password" placeholder="{{ __('Contraseña') }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row mb-0">
          <div class="col-md-12">
              @if (Route::has('password.request'))
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                      {{ __('Forgot Your Password?') }}
                  </a>
              @endif
          </div>
      </div>

      <div class="form-group row mb-0">
          <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-lg btn-block">
                  {{ __('Iniciar Sesión') }}
              </button>
          </div>
      </div>
      <p class="mt-5 mb-3 text-muted">© 2019 Fundación Moisés todos los derechos reservados | Desarrollado por: <a href="https://fdevelopershn.com/">Freelance Developers</a></p>
  </form>
@endsection
