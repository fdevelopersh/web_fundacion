@extends('layouts.main')
@section('title', 'Producción y distribución de pollos, hortalizas, tilapia, café y miel')
@section('urlcanonical','https://productosmoises.com/')
@section('description', 'Contamos con la más alta tecnología para la producción de alimentos y demás procesos necesarios con el fin de hacer llegar a sus casas un producto de mayor calidad posible.')
@section('keywords', 'Producción, comercialización y distrubución de pollos, hortalizas, tilapia, café, miel')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Comercialización y distribución de pollos, hortalizas, tilapia, café y miel')
@section('ogurl', 'https://productosmoises.com')
@section('ogimage', 'https://productosmoises.com/img/productos-moises-pollo.jpg')
@section('ogdescription', 'Productos naturales y orgánicos cultivados a través de un proceso natural.')
{{-- End For FB Meta tags --}}
@section('content')
    <header>
      <div class="slider fullscreen">
        <ul class="slides">
          @foreach ($sliders as $slider)
          <li>
              <img src="/img/slider/{{ $slider->img_slider }}" alt="{{ $slider->img_alt }}"> <!-- random image -->
              <div class="caption {{ $slider->align_text }}-align">
                <h2>{{ $slider->title }}</h2>
                <h5 class="light grey-text text-lighten-3">{{ $slider->subtitle }}</h5>
              </div>
            </li>
          @endforeach
        </ul>
      </div>
    </header>
    <section id="about" class="bg-white">
        <div class="content-wrapper">
          <div class="post container">
            <div class="row">
              <div class="col s12 l12 m12">
                <h1 class="center-align title">¿Quiénes somos?</h1>
                <div class="separate"></div>
                <div class="content-text-p">
                  <p class="center-align color-dark-content">Productos Moisés es un departamento agroalimentario de Misión Hacia Arriba. Nos dedicamos
                  a la producción, comercialización y distribución de pollos, hortalizas, tilapia, café y miel. Uno de nuestros objetivos es hacer
                  llegar a nuestros clientes productos con la más alta calidad y máxima frescura. Contamos con la más alta tecnología para la
                  producción de alimentos y demás procesos necesarios con el fin de hacer llegar a sus casas un producto de mayor calidad posible.</p>
                </div>
                <div class="center-align"><a href="/nosotros"><button class="button">Ver Más...</button></a></div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <section id="img-phrase">
        <div class="row">
          <div class="col s12 l4 m4 no-padding">
            <div class="img-full img-1"></div>
          </div>
          <div class="col s12 l4 m4 no-padding">
            <div class="img-full img-2"></div>
          </div>
          <div class="col s12 l4 m4 no-padding">
            <div class="img-full img-3"></div>
          </div>
        </div>
    </section>
    <section id="count-section" class="bg-white">
        <div class="content-wrapper">
          <div class="post container">
            <div class="row">
              <div class="col s12 l12 m12">
                <h3 class="center-align title">Forma parte del cambio, consume productos Moisés.</h3>
                <div class="separate"></div>
              </div>
              <div class="col s12 l3 m6">
                <div class="count center-align color-dark-content">
                  <span class="count-num">150</span>
                  <span>+ Empleos Generados</span>
                </div>
              </div>
              <div class="col s12 l3 m6">
                <div class="count center-align color-dark-content">
                  <span class="count-num">500,000</span>
                  <span>+ Ingresos a pequeños productores</span>
                </div>
              </div>
              <div class="col s12 l3 m6">
                <div class="count center-align color-dark-content">
                  <span class="count-num">20,000</span>
                  <span>+ Clientes Sastifechos</span>
                </div>
              </div>
              <div class="col s12 l3 m6">
                <div class="count center-align color-dark-content">
                  <span class="count-num">50</span>
                  <span>+ Jóvenes siendo formados</span>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <section id="parallax-phrase">
      <div class="parallax-container">
        <div class="container">
          <div class="content-wrapper">
            <div class="row">
              <div class="col s12 m12 l12 center-align">
                <div class="content-text-p">
                  <h3 class="title-parallax">Productos Moisés</h3>
                  <p class="content-parallax">Al comprar nuestros Productos Moisés, usted forma parte de la capacitación y el equipamiento de las
                  futuras generaciones de empresarios, dueños de negocios y líderes comunitarios, que darán forma y transformarán a Honduras.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="parallax"><img src="./img/hortalizas-moises.webp" alt="hortalizas moisés"></div>
      </div>
    </section>
    <section id="products" class="bg-white">
      <div class="content-wrapper">
        <div class="post container">
          <h5 class="center-align no-margin sub-title">¿Qué ofrecemos?</h5>
          <h3 class="center-align title no-margin">Variedad de productos frescos</h3>
          <div class="separate"></div>
          <div class="row">
              <div class="gallery-img">
                <img class="img-gallery" src="/img/productos-moises-cafe.webp" alt="productos-moisés-café">
                <span class="title-gallery center-align">Distribución a nivel nacional </span>
                <span class="text">Venta de diferentes productos frescos.</span>
              </div>
              <div class="gallery-img">
                <img class="img-gallery" src="img/productos-moises-tilapia.webp" alt="productos-moisés-tilapia">
                <span class="title-gallery center-align">Productos de calidad</span>
                <span class="text">Higiene, calidad, excelencia.</span>
              </div>
              <div class="gallery-img">
                <img class="img-gallery" src="img/productos-moises-frescos.webp" alt="productos-frescos">
                <span class="title-gallery center-align">Innovación y mejora continua</span>
                <span class="text">Nuevas técnicas y tecnologías agrícolas.</span>
              </div>
              <div class="gallery-img">
                <img class="img-gallery" src="img/pollo-moises-producto.webp" alt="pollo-moisés">
                <span class="title-gallery center-align">Productos naturales y orgánicos.</span>
                <span class="text">Contribuyendo a su calidad de vida.</span>
              </div>
              <div class="gallery-img">
                <img class="img-gallery" src="img/hortalizas-naturales.webp" alt="hortalizas-naturales">
                <span class="title-gallery center-align">Siembra y cosecha.</span>
                <span class="text">Alimentos saludables.</span>
              </div>
              <div class="gallery-img">
                <img class="img-gallery" src="img/miel-moises.webp" alt="miel-moisés">
                <span class="title-gallery center-align">Apoyando jóvenes.</span>
                <span class="text">Fomentando valores espirituales.</span>
              </div>
            </div>
          </div>
          <div class="center-align"><a href="/productos"><button class="button">Ver Más...</button></a></div>
        </div>
    </section>
    <section id="testimonio" class="bg-gray">
        <div class="content-wrapper">
          <div class="post container">
            <div class="row no-margin">
              <div class="col s12 l12 m12">
                <h5 class="center-align no-margin sub-title">Testimonios</h5>
                <h3 class="center-align title no-margin">¿Qué dicen nuestros clientes?</h3>
                <div class="separate"></div>
              </div>
              <div class="col s12 l12 m12">
                <div class="testimonials">
                  <div class="slider_testimonial">
                    @foreach ($testimonials as $testimonial)
                     <div class="card">
                       <div class="card-content-testimonial">
                         <div class="slider-1">
                           <img class="img-avatar circle" src="/img/customers-testimonial/{{ $testimonial->img_testimonial }}" alt="{{ $testimonial->name }}">
                           <p class="center-align">{{ $testimonial->content }}</p>
                            <h4 class="client-name">{{ $testimonial->name }}</h4>
                            <p class="client-cargo">{{ $testimonial->job_title }}</p>
                         </div>
                       </div>
                     </div>
                     @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <section id="gallery" class="bg-white">
      <div class="content-wrapper">
        <div class="post container">
          <h5 class="center-align no-margin sub-title">Conocenos un poco más</h5>
          <h3 class="center-align title no-margin">Galería de imágenes</h3>
          <div class="separate"></div>
            <div id="gallery" class="center-align">
              <ul>
                <li id="image1">
                  <div class="mosaicItem"><a href="#popin1">
                      <img src="./img/pollos-moises-galeria.webp" alt="pollo-moisés-galeria" />
                      <div class="text">description lorim</div>
                    </a></div>
                  <div class="popin" id="popin1"><a href="#image1">
                      <div class="overlay"></div>
                      <div class="imgBox"><img src="./img/pollos-moises-galeria.webp" alt="pollo-moisés-galeria" /></div>
                    </a></div>
                </li>
                <li id="image2">
                  <div class="mosaicItem"><a href="#popin2">
                      <img src="./img/hortalizas-moises-galeria-home.webp" alt="hortalizas-moises-galeria"/>
                      <div class="text">description lorim</div>
                    </a></div>
                  <div class="popin" id="popin2"><a href="#image2">
                      <div class="overlay"></div>
                      <div class="imgBox"><img src="./img/hortalizas-moises-galeria-home.webp" alt="hortalizas-moises-galeria" /></div>
                    </a></div>
                </li>
                <li id="image3">
                  <div class="mosaicItem"><a href="#popin3">
                      <img src="./img/miel-moises-galeria-home.webp" alt="miel-moises-galeria" />
                      <div class="text">description lorim</div>
                    </a></div>
                  <div class="popin" id="popin3"><a href="#image3">
                      <div class="overlay"></div>
                      <div class="imgBox"><img src="./img/miel-moises-galeria-home.webp" alt="miel-moises-galeria" /></div>
                    </a></div>
                </li>
              </ul>
            </div>
          <div class="center-align"><a href="/galeria"><button class="button">Ver Más...</button></a></div>
        </div>
      </div>
    </section>
    <section id="Partners" class="bg-gray">
        <div class="content-wrapper">
          <div class="post container">
            <div class="row no-margin">
              <div class="col s12 l12 m12">
                <h5 class="center-align no-margin sub-title">Encuéntranos</h5>
                <h3 class="center-align title no-margin">En diferentes supermercados a nivel nacional</h3>
                <div class="separate"></div>
                <div class="row no-margin padding-top-buttom">
                  @foreach ($partners as $partner)
                  <div class="col s12 l3 m6">
                    <img class="img-partners" src="/img/partners/{{ $partner->img_partner }}" alt="{{ $partner->img_description }}">
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <section id="blog" class="bg-white">
        <div class="content-wrapper">
          <div class="post container">
            <h5 class="center-align no-margin sub-title">Últimas noticias</h5>
            <h3 class="center-align title no-margin">Blog de noticias</h3>
            <div class="separate"></div>
            <div class="row">
              @foreach ($blogs as $key => $blog)
                @if ($key%2 != 0)
              <div class="blog-card">
                  @else
              <div class="blog-card alt">
                @endif
                <div class="meta">
                  <img class="photo" src="/img/blog/{{ $blog->image}}" alt="{{ $blog->slug}}">
                  <ul class="details">
                    <li class="author">Fundación Moisés</li>
                    <li class="date">{{ $blog->created_at->format('M. j, Y') }}</li>
                  </ul>
                </div>
                <div class="description">
                  <h1>{{ $blog->title }}</h1>
                  <h2>{{ $blog->subtitle }}</h2>
                  <p>{!! strip_tags(str_limit(html_entity_decode($blog->content),175)) !!}</p>
                  <p class="read-more">
                    <a href="blog/{{ $blog->slug}}">Read More</a>
                  </p>
                </div>
              </div>
              @endforeach
            </div>
            <div class="center-align"><a href="/blog"><button class="button">Ver Más...</button></a></div>
          </div>
        </div>
    </section>
@endsection
