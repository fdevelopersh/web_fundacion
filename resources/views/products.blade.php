@extends('layouts.global_main')
@section('title', 'Venta de productos frescos, orgánicos y saludables')
@section('urlcanonical','https://productosmoises.com/productos')
@section('description', 'Líderes en el mercado regional de la venta de productos frescos, motivando la creación de consumidores inteligentes que buscan las opciones de alimento más saludable.')
@section('keywords', 'productos saludables, productos frescos, productos orgánicos')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Producción de productos orgánicos y frescos | Productos Moisés')
@section('ogurl', 'https://productosmoises.com/productos')
@section('ogimage', 'https://productosmoises.com/img/equipo-moises.jpg')
@section('ogdescription', 'Lideres en el mercado regional en la venta de productos frescos')
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Productos Moisés</h1>
        <div class="parallax"><img src="./img/productos-moises-pollo.jpg" alt="productos-moises-pollos"></div>
      </div>
  </section>
  <section id="products" class="bg-white">
    <div class="content-wrapper">
        <div class="container">
          <h5 class="center-align no-margin sub-title">Calidad es nuestra prioridad</h5>
          <h3 class="center-align title no-margin">Nuestros productos</h3>
          <div class="separate"></div>
          <div class="row">
            @foreach ($products as $product)
            <div class="col s12 m6 l3">
                <div class="card medium hoverable">
                  <div class="card-image">
                    <img src="/img/products/{{ $product->image_product}}" alt="{{ $product->image_alt}}">
                    <span class="card-title">{{ $product->title}}</span>
                  </div>
                  <div class="card-content">
                     {!! strip_tags(str_limit(html_entity_decode($product->content_product),143)) !!}
                     </div>
                  <div class="card-action center-align">
                    <a href="/productos/{{ $product->slug }}">Leer Más...</a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
    </div>
  </section>
  <section id="ourhistory" class="bg-gray">
      <div class="content-wrapper">
        <div class="post container">
            <div class="row">
              <div class="col s12 m12 l6">
                <img src="./img/producto-natural.jpeg" class="responsive-img" alt="productos-naturales">
              </div>
              <div class="col s12 m12 l6">
                <h5 class="center-align no-margin sub-title">Productos de alta calidad</h5>
                <h3 class="center-align title no-margin">Producto 100% Natural</h3>
                <div class="separate"></div>
                <div>
                  <p class="justify-align">El Proyecto Moisés es una forma a largo plazo de tratar de lograr un cambio en las comunidades pobres y en todas las áreas
          				con las que cuenta. Al consumir Productos Moisés permites a los jóvenes, que de otro modo se verían obligados a abandonar la escuela, la oportunidad
          				de continuar su educación secundaria. Además, recibir capacitación espiritual a medida que se les enseña trabajo útil y habilidades para la vida.</p>
          				<p class="justify-align">Productos Moisés es una marca de propiedad absoluta de Mission UpReach, Inc., una organización sin fines de lucro 501.c.3
          				Para obtener más información sobre todos los otros trabajos que realiza Mission UpReach, visítenos en <a href="https://missionupreach.org/">missionupreach.org</a></p>
                </div>
                <div class="center-align"><a href="/contactanos"><button class="button">Contáctanos</button></a></div>
              </div>
            </div>
        </div>
      </div>
  </section>
@endsection
