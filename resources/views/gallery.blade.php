@extends('layouts.global_main')
@section('title', 'Galería de imágenes - Productos Moisés')
@section('urlcanonical','https://productosmoises.com/galeria')
@section('description', 'Conoce un poco más sobre Productos Moisés')
@section('keywords', 'Galería de imágenes, Productos Moisés')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Galería de imágenes y videos | Productos Moisés')
@section('ogurl', 'https://productosmoises.com/galeria')
@section('ogimage', 'https://productosmoises.com/img/galeria-imagenes-productos-moises.jpg')
@section('ogdescription', 'Conoce más sobre Productos Moisés')
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Galería de imágenes</h1>
        <div class="parallax"><img src="./img/galeria-imagenes-productos-moises.jpg" alt="galeria-imagenes-productos-videos"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
    <div class="content-wrapper">
        <div class="container">
          <h5 class="center-align no-margin sub-title">Conocenos un poco más</h5>
          <h3 class="center-align title no-margin">Galería de imágenes y videos</h3>
          <div class="row">
             <div class="col s12">
               <ul class="tabs">
                 <li class="tab col s2 m6 l2 offset-l4"><a class="active" href="#test1">Imagenes</a></li>
                 <li class="tab col s2 m6 l2"><a href="#test2">Videos</a></li>
               </ul>
             </div>
             <div id="test1" class="col s12">
               <div class="row">
                 <div class="filter">
                  @foreach ($images as $image)
                  <div class="col s12 m4 l4 padding-filtering-col filtr-item" data-category="1">
                      <img src="/img/gallery/images/{{ $image->image }}" class="responsive-img materialboxed" data-caption="{{ $image->image_text }}" alt="{{ $image->image_alt }}">
                  </div>
                   @endforeach
                 </div>
               </div>
             </div>
             <div id="test2" class="col s12">
               <div class="row">
                  @foreach ($videos as $video)
                 <div class="col s12 l4 m6 padding-filtering-col">
                    @php
                    $linkvideo = substr( $video->link_video,32 );
                    @endphp
                   <div class="video-container">
                     <iframe width="853" height="480" src="https://www.youtube.com/embed/{{ $linkvideo }}" frameborder="0" allowfullscreen></iframe>
                   </div>
                 </div>
                 @endforeach
               </div>
             </div>
          </div>
        </div>
    </div>
  </section>
@endsection
