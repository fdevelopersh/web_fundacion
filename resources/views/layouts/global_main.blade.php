<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    @include('includes._head')
  </head>
  <body>
    @include('includes._menu_general')
    @yield('content')
    @include('includes._scripts')
    @include('includes._footer')
  </body>
</html>
