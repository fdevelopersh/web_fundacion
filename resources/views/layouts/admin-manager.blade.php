<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('includes.admin-manager._head')
  </head>
  <body>
    <header>
    @include('includes.admin-manager._header')
    </header>
    <main>
      <div class="admin-container">
         {{-- Warning message --}}
         @if ($message = Session::get('warning'))
           <script type="text/javascript">
              Swal.fire({
                 icon: 'error',
                 title:'Warning!',
                 html:"<p>{{ $message }}</p>"
              })
           </script>
         @endif
         {{-- Sucess message --}}
         @if ($message = Session::get('success'))
           <script type="text/javascript">
              Swal.fire({
                 icon: 'success',
                 title:'Success!',
                 html:"<p>{{ $message }}</p>"
              })
           </script>
         @endif
         {{-- danger message --}}
         @if ($message = Session::get('danger'))
           <script type="text/javascript">
              Swal.fire({
                 icon: 'success',
                 title:'Success!',
                 html:"<p>{{ $message }}</p>"
              })
           </script>
         @endif
        @yield('content')
      </div>
    </main>
    @include('includes.admin-manager._scripts')
  </body>
</html>
