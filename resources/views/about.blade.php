@extends('layouts.global_main')
@section('title', '¿Quiénes somos? - Productos Moisés')
@section('urlcanonical','https://productosmoises.com/nosotros')
@section('description', 'Productos Moisés es un departamento agroalimentario de Misión Hacia Arriba. Nos dedicamos a la producción, comercialización y distribución de pollos, hortalizas, tilapia, café y miel.')
@section('keywords', 'productos orgánicos, productos frescos')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Productos 100% orgánicos y frescos | Productos Moisés')
@section('ogurl', 'https://productosmoises.com/nosotros')
@section('ogimage', 'https://productosmoises.com/img/productos-moises-about.jpg')
@section('ogdescription', 'Nos dedicamos a la producción, comercialización y distribución de pollos, hortalizas, tilapia, café y miel.')
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Productos Moisés</h1>
        <div class="parallax"><img src="./img/productos-moises-about.webp" alt="acerca-de-productos-moisés"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
        <div class="content-wrapper">
          <div class="container">
            <div class="row">
              <div class="col s12 l6 m12">
                <h5 class="center-align no-margin sub-title">Conoce más</h5>
                <h3 class="center-align title no-margin">Sobre nosotros</h3>
                <div class="row">
                  <div class="col s12">
                    <ul class="tabs">
                      <li class="tab col s4"><a class="active" href="#test1">Misión</a></li>
                      <li class="tab col s4"><a href="#test2">Nosotros</a></li>
                      <li class="tab col s4"><a href="#test4">Visión</a></li>
                    </ul>
                  </div>
                  <div id="test1" class="col s12 justify-align margin-content-tabs">Ofrecer a nuestros clientes productos
                  naturales y orgánicos cultivados a través de un proceso natural realizado mediante formación de servicio educativo, moral y
                  espiritual de nuestros colaboradores contribuyendo a una vida saludable y plena, y lograr un bienestar en medio de un ambiente
                  moderno y ritmo de vida frenético.</div>
                  <div id="test2" class="col s12 justify-align margin-content-tabs">Productos Moisés es un departamento agroalimentario de Misión Hacia
                  Arriba. Nos dedicamos a la producción, comercialización y distribución de pollos, hortalizas, tilapia, café y miel. Uno de nuestros
                  objetivos es hacer llegar a nuestros clientes productos con la más alta calidad y máxima frescura. Contamos con la más alta
                  tecnología para la producción de alimentos y demás procesos necesarios con el fin de hacer llegar a sus casas un producto de mayor
                  calidad posible.<br><br>El Proyecto y productos Moisés, más allá de cultivar y distribuir productos, formar jóvenes fieles a Cristo Jesús
                  es nuestro motor más grande y nuestra fuerza para continuar hacia adelante, así como proveer para los hombres un ministerio entre
                  los muchachos. Tiene como meta instruir a los jóvenes en el camino correcto, orientarlos para el servicio a nuestro país formando
                  bases de rectitud e integridad en ellos, inspirarlos para crecer en su vida en todo aspecto, enseñándoles a trabajar, fomentar la
                  unidad y desarrollar sus talentos, habilidades, conocimientos y que ellos puedan desarrollarse para un futuro regresar a sus hogares
                  y ser hombres firmes, rectos que darán frutos de todo lo que aprendieron en el proyecto Moisés</div>
                  <div id="test4" class="col s12 justify-align margin-content-tabs">Ser líderes en el mercado regional de la venta de productos frescos,
                  motivando la creación de consumidores inteligentes que buscan las opciones de alimento más saludable y orgánico de alimento para
                  contribuir a su calidad de vida.</div>
                </div>
              </div>
              <div class="col s12 l6 m12 center-align">
                <img src="./img/acerca-de-productos-moises.webp" class="responsive-img" alt="productos-moises">
              </div>
            </div>
          </div>
        </div>
  </section>
  <section id="ourhistory" class="bg-gray">
      <div class="content-wrapper">
        <div class="post container">
            <div class="row">
              <div class="col s12 m12 l6 center-align">
                <img src="./img/productos-moises-sabiasque.webp" class="responsive-img" alt="sabiasque-productosmoises">
              </div>
              <div class="col s12 m12 l6">
                <h5 class="center-align no-margin sub-title">Dato</h5>
                <h3 class="center-align title no-margin">¿Sabías que?</h3>
                <div class="separate"></div>
                <div>
                  <p class="justify-align">El proyecto Moisés consta de 120 manzanas en una pequeña comunidad a las afueras de Santa Rosa de Copán.
                  Esta instalación alberga un promedio de 40 niños mayores de 13 años. Estos jóvenes realizan prácticas en los aspectos técnicos y
                  económicos de la agricultura; específicamente en caficultura, avicultura, producción avícola, producción de hortalizas y apicultura,
                  así también, procesamiento de residuos para elaboración de compost.</p>
                </div>
                <div class="center-align"><a href="/nosotros/historia"><button class="button">Ver Más...</button></a></div>
              </div>
            </div>
        </div>
      </div>
  </section>
  <section id="Ourteam" class="bg-white">
    <div class="content-wrapper">
      <div class="post container">
        <div class="row">
            <div class="heading-title text-center">
                <h5 class="center-align no-margin sub-title">Conoce nuestro equipo Moisés</h5>
                <h3 class="center-align title no-margin">Personas que aman su trabajo</h3>
                <div class="separate"></div>
                @foreach ($teams as $team)
                <div class="col s12 m6 l4">
                    <div class="team-member">
                        <div class="team-img">
                            <img src="/img/team/{{ $team->image }}" alt="	{{ $team->image_description }}" class="img-responsive-team">
                        </div>
                        <div class="team-hover">
                            <div class="desk">
                              <h4>{{ $team->greeting_title }}</h4>
                              <p>{{ $team->phrase }}</p>
                              <a href="/nosotros/equipo/{{ $team->slug }}">Leer Más...</a>
                            </div>
                        </div>
                    </div>
                    <div class="team-title">
                        <h5>{{ $team->name_team }}</h5>
                        <span>{{ $team->job_title }}</span>
                    </div>
                  </div>
                @endforeach
            </div>
          </div>
        </div>
    </div>
 </section>
@endsection
