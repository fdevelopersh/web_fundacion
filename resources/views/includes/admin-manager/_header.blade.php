    <nav id="nav-admin">
      <div class="nav-wrapper">
        <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <a href="/fwdfundacion/admin-panel"><span class="white-text title-navbar-admin">Admin Panel Fundación Moisés</span></a>
      </div>
    </nav>
    <ul id='dropdown1' class='dropdown-content'>
      <li><a href="/fwdfundacion/admin-panel/users/edit/{{ Auth::user()->id }}">Editar Perfil</a></li>
          @auth
          <li>
          <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"
          {{ __('Logout') }}>Salir</a></li>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

        @endauth
    </ul>
    <ul id="slide-out" class="sidenav sidenav-fixed">
        <li><div class="user-view">
          <div class="background">
            <img src="/img/background/{{ Auth::user()->image_background }}">
          </div>
          <a href="/fwdfundacion/admin-panel/users/edit/{{ Auth::user()->id }}"><img class="circle" src="/img/users/{{ Auth::user()->image_user }}"></a>
          <a href="/fwdfundacion/admin-panel/users/edit/{{ Auth::user()->id }}"><span class="white-text name">{{ Auth::user()->name }}</span></a>
          <a href="/fwdfundacion/admin-panel/users/edit/{{ Auth::user()->id }}"><span class="white-text email">{{ Auth::user()->email }}</span></a>
          <a class='dropdown-trigger' data-target='dropdown1'><button class="buttonp">Cuenta</button></a>
        </div></li>
        <li><a href="/fwdfundacion/admin-panel/"><i class="material-icons">dashboard</i>Admin Panel</a></li>
        <li><div class="divider"></div></li>
        <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-teal"><i class="material-icons">home</i>Inicio</a>
             <div class="collapsible-body">
               <ul>
                 <li><a href="/fwdfundacion/admin-panel/slider"><i class="material-icons">burst_mode</i>Slider</a></li>
                 <li><a href="/fwdfundacion/admin-panel/clientes-testimonios"><i class="material-icons">person</i>Testimonios de Clientes</a></li>
                 <li><a href="/fwdfundacion/admin-panel/patrocinadores"><i class="material-icons">shop_two</i>Patrocinadores</a></li>
               </ul>
             </div>
           </li>
          </ul>
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-teal"><i class="material-icons">business</i>Nosotros</a>
             <div class="collapsible-body">
               <ul>
                 <li><a href="/fwdfundacion/admin-panel/about/team"><i class="material-icons">group</i>Equipo de Trabajo</a></li>
               </ul>
             </div>
           </li>
          </ul>
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-teal"><i class="material-icons">photo_album</i>Galería</a>
             <div class="collapsible-body">
               <ul>
                 <li><a href="/fwdfundacion/admin-panel/galeria/imagenes"><i class="material-icons">photo</i>Imagenes</a></li>
                 <li><a href="/fwdfundacion/admin-panel/galeria/videos"><i class="material-icons">video_library</i>Videos</a></li>
               </ul>
             </div>
           </li>
          </ul>
        </li>
        <li><a href="/fwdfundacion/admin-panel/products" class="padding-li-sidenav"><i class="material-icons">business_center</i>Productos</a></li>
        <li><a href="/fwdfundacion/admin-panel/blog" class="padding-li-sidenav"><i class="material-icons">art_track</i>Noticias</a></li>
        @if (Auth::user()->type_user==1)
        <li><a href="/fwdfundacion/admin-panel/users" class="padding-li-sidenav"><i class="material-icons">person</i>Usuarios</a></li>
        @endif
     </ul>
