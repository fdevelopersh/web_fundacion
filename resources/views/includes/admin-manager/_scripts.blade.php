    <script type="text/javascript" src="/js/main.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
          $('.sidenav').sidenav();
          $('.collapsible').collapsible();
          $('.dropdown-trigger').dropdown();
          $('select').formSelect();
          $('textarea.textarea2').characterCounter();
        });
    </script>
