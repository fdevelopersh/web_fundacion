    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="canonical" href="@yield('urlcanonical')"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/main.css')}}" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

    <meta name="twitter:card" content="summary" />
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="Freelance Developers">
    <meta property="og:title" content="@yield('ogtitle')" />
    <meta property="og:type" content="@yield('ogtype', 'website')" />
    <meta property="og:url" content="@yield('ogurl')" />
    <meta property="og:image" content="@yield('ogimage')" />
    <meta property="og:site_name" content="Productos Moisés" />
    <meta property="og:description" content="@yield('ogdescription')" />

    <script type="application/ld+json">{ "@context": "https://schema.org/","@type": "WebSite","name": "Productos Moisés","url": "https://productosmoises.com/","potentialAction": { "@type": "SearchAction","target": "{search_term_string}","query-input": "required name=search_term_string"}}</script>
    <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
    <script>
        window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
        "popup": {
            "background": "#01090f",
            "text": "#fffcfc"
        },
        "button": {
            "background": "#e75b4c",
            "text": "#ffffff"
        }
        },
        "content": {
        "message": "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.",
        "dismiss": " Lo tengo!",
        "link": "Aprende más"
        }
    })});
    </script>
