<section id="footer">
    <footer class="page-footer">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Productos Moisés</h5>
            <p class="grey-text text-lighten-4 justify-align">El Proyecto y productos Moisés, más allá de cultivar y distribuir productos, forma jóvenes fieles
            a Cristo Jesús quien es nuestro motor más grande y nuestra fuerza para continuar hacia adelante, así como proveer para los hombres un ministerio
            entre los muchachos.</p>
            <div class="social-media-padding-top">
              <a href="https://www.facebook.com/productosmoises"><img class="social-media-m" src="/img/facebook.png"
                  alt="Icono facebook"></a>
              <!--<a href="#"><img class="social-media-m" src="/img/twitter.png"
                  alt="Icono twitter"></a>-->
              <a href="https://www.instagram.com/productosmoises/"><img class="social-media-m" src="/img/instagram.png"
                  alt="Icono instagram"></a>
              <a href="https://www.youtube.com/channel/UCPQ_w4Xa1sgjYgIJ9OWbj2A"><img class="social-media-m" src="/img/youtube.png"
                  alt="Icono youtube"></a>
            </div>
          </div>
          <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Contáctanos</h5>
            <p class="left-align"><img class="social-media-s" src="/img/ubicacion.png" alt="Icono ubicacion"> El Carrizal, Santa Rosa de Copán Honduras C.A</p>
            <p class="left-align"><img class="social-media-s" src="/img/whatsapp.png" alt="Icono whatsapp"> (+504) 2662-1533</p>
            <p class="left-align"><img class="social-media-s" src="/img/whatsapp.png" alt="Icono whatsapp"> (+504) 9491-6427</p>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container center-align">
        <p>© 2019 Productos Moisés todos los derechos reservados | Desarrollado por <a href="https://fdevelopershn.com/">Freelance Developers</a></p>
        </div>
      </div>
    </footer>
</section>
