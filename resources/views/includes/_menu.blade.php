    <nav>
      <div class="container nav-wrapper">
        <a href="/" class="brand-logo">Logo</a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="/">Inicio</a></li>
            <li><a href="/nosotros">Nosotros</a></li>
            <li><a href="/productos">Productos</a></li>
            <li><a href="/galeria">Galeria</a></li>
            <li><a href="/blog">Noticias</a></li>
            <li><a href="/contactanos">Contáctanos</a></li>
        </ul>
      </div>
    </nav>
    <ul class="sidenav" id="mobile-demo">
        <li><a href="/"><i class="material-icons">home</i>Inicio</a></li>
        <li><a href="/nosotros"><i class="material-icons">business</i>Nosotros</a></li>
        <li><a href="/productos"><i class="material-icons">business_center</i>Productos</a></li>
        <li><a href="/galeria"><i class="material-icons">photo_album</i>Galeria</a></li>
        <li><a href="/blog"><i class="material-icons">art_track</i>Noticias</a></li>
        <li><a href="/contactanos"><i class="material-icons">email</i>Contáctanos</a></li>
    </ul>
