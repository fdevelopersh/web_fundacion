@extends('layouts.global_main')
@section('title', 'Noticias sobre productos Moisés')
@section('urlcanonical','https://productosmoisescom/blog')
@section('description', 'Cambiando vidas a través del alcance holístico: físico, espiritual y emocional.')
@section('keywords', 'blog, noticias, productos, producción')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Noticias | Productos Moisés')
@section('ogurl', 'https://productosmoises.com/blog')
@section('ogimage', 'https://productosmoises.com/img/productos-moises-noticias.jpg')
@section('ogdescription', 'Cambiando vidas a través del alcance holístico: físico, espiritual y emocional.')
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Noticias</h1>
        <div class="parallax"><img src="/img/productos-moises-noticias.jpg" alt="blog-noticias"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
    <div class="content-wrapper">
        <div class="container">
          <h5 class="center-align no-margin sub-title">Últimas noticias</h5>
          <h3 class="center-align title no-margin">Blog de noticias</h3>
          <div class="separate"></div>
          <div class="row">
            @foreach ($blogs as $key => $blog)
              @if ($key%2 != 0)
            <div class="blog-card">
                @else
            <div class="blog-card alt">
              @endif
              <div class="meta">
                <img class="photo" src="/img/blog/{{ $blog->image}}" alt="{{ $blog->slug}}">
                <ul class="details">
                  <li class="author">Fundación Moisés</li>
                  <li class="date">{{ $blog->created_at->format('M. j, Y') }}</li>
                </ul>
              </div>
              <div class="description">
                <h1>{{ $blog->title }}</h1>
                <h2>{{ $blog->subtitle }}</h2>
                <p>{!! strip_tags(str_limit(html_entity_decode($blog->content),175)) !!}</p>
                <p class="read-more">
                  <a href="blog/{{ $blog->slug}}">Read More</a>
                </p>
              </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col s12 m12 l12 center-align">
              {{ $blogs->render('manager.pagination_materialize') }}
            </div>
          </div>
        </div>
    </div>
  </section>
@endsection
