@extends('layouts.global_main')
@section('title', '¿Sabías que? - Productos Moisés')
@section('urlcanonical','https://productosmoises.com/historia')
@section('description', 'El proyecto Moisés consta de 120 manzanas en una pequeña comunidad a las afueras de Santa Rosa de Copán. Esta instalación alberga un promedio de 40 niños mayores de 13 años.')
@section('keywords', 'Proyecto Moisés, Productos Moisés')
{{-- For FB Meta tags --}}
@section('ogtitle', '¿Sabías que? - Productos Moisés')
@section('ogurl', 'https://productosmoises.com/historia')
@section('ogimage', 'https://productosmoises.com/img/fundacion-about-m.jpg')
@section('ogdescription', 'El objetivo principal de proyecto Moisés es fomentar los valores espirituales, el área espiritual y moral.')
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Productos Moisés</h1>
        <div class="parallax"><img src="/img/fundacion-about-m.jpg" alt="fundacion-Moisés-about"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
        <div class="content-wrapper">
          <div class="container">
            <div class="row">
              <div class="col s12 l12 m12">
                <h5 class="center-align no-margin sub-title">Dato Importante</h5>
                <h3 class="center-align title no-margin">¿Sabías que?</h3>
                <div class="content-text-p">
                  <p class="justify-align color-dark-content">El proyecto Moisés consta de 120 manzanas en una pequeña comunidad a las afueras de
                  Santa Rosa de Copán. Esta instalación alberga un promedio de 40 niños mayores de 13 años. Estos jóvenes realizan prácticas en
                  los aspectos técnicos y económicos de la agricultura; específicamente en caficultura, avicultura, producción avícola,
                  producción de hortalizas y apicultura, así también, procesamiento de residuos para elaboración de compost. La mayoría de estos
                  jóvenes, debido a la falta de finanzas o centros educativos en sus comunidades, no hubieran podido continuar con su educación.
                  A través de este programa, hombres jóvenes de todo el occidente de Honduras están siendo entrenados como líderes innovadores. </p>
                  <p class="justify-align color-dark-content">Proyecto Moisés no solo se dedica a la producción, distribución y venta de diferentes productos. El objetivo principal es
                  fomentar los valores espirituales, el área espiritual y moral es de suma importancia para nosotros, por eso, sumándole a su
                  capacitación laboral, esos principios se han integrado en nuestra metodología para que cada programa y esfuerzo en el que
                  invertimos tiempo, dinero y energía sean con el fin de lograr formar jóvenes y personas conocedoras de Dios, integras y rectas
                  en su caminar.</p>
                  <p class="justify-align color-dark-content">Al comprar nuestros Productos Moisés, usted forma parte de la capacitación y el equipamiento de las futuras generaciones de empresarios,
                  dueños de negocios y líderes comunitarios, que darán forma y transformarán a Honduras.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
  </section>
@endsection
