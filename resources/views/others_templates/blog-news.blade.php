@extends('layouts.global_main')
@section('title') {{ $blog->title }} @endsection
@section('urlcanonical','https://productosmoises.com/blog')
@section('description'){!! strip_tags(html_entity_decode(str_limit( $blog->subtitle, 82))) !!}@endsection
@section('keywords', 'blog, noticias, productos, producción')
{{-- For FB Meta tags --}}
@section('ogtitle') {!! $blog->title !!} | Productos Moisés @endsection
@section('ogurl') https://productosmoises.com/blog/{{ $blog->slug }} @endsection
@section('ogimage') https://productosmoises.com/img/blog/{{ $blog->image }}@endsection
@section('ogdescription') {!! strip_tags(html_entity_decode(str_limit( $blog->subtitle, 82))) !!}@endsection
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-blog">{{ $blog->title }}</h1>
        <div class="parallax"><img src="/img/blog/{{ $blog->image }}" alt="{{ $blog->image_alt}}"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
        <div class="content-wrapper">
          <div class="container">
            <div class="row">
              <div class="col s12 l12 m12">
                <h3 class="title-blog no-margin">{{ $blog->subtitle }}</h3>
                <div class="post-info">
                 <span>{{ $blog->created_at->format('l j F, Y') }}</span>
                 <span>Por: Productos Moisés</span>
               </div>
                <p class="justify-align color-dark-content">{!! $blog->content !!}</p>
              </div>
            </div>
          </div>
        </div>
  </section>
@endsection
