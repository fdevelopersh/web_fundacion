@extends('layouts.global_main')
@section('title') {{ $product->title }} - Productos Moisés @endsection
@section('urlcanonical','https://productosmoises.com/productos')
@section('description'){!! strip_tags(html_entity_decode(str_limit( $product->content_product, 82))) !!}@endsection
@section('keywords', 'cultivo tilapia, miel, café, pollo moisés, hortalizas')
{{-- For FB Meta tags --}}
@section('ogtitle') {!! $product->title !!} | Productos Moisés @endsection
@section('ogurl') https://productosmoises.com/productos/{{ $product->slug }} @endsection
@section('ogimage') https://productosmoises.com/img/products/{{ $product->image_product }}@endsection
@section('ogdescription') {!! strip_tags(html_entity_decode(str_limit( $product->content_product, 82))) !!}@endsection
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">{{ $product->title }}</h1>
        <div class="parallax"><img src="/img/products/{{ $product->image_product }}" alt="{{ $product->image_alt }}"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
        <div class="content-wrapper">
          <div class="container">
            <div class="row">
              <h5 class="center-align no-margin sub-title">Conoce más</h5>
              <div class="col s12 l12 m12">
                <h3 class="center-align title no-margin">{{ $product->content_title }}</h3>
                <div class="content-text-p">
                  <p class="justify-align color-dark-content">{!! $product->content_product !!}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
  </section>
@endsection
