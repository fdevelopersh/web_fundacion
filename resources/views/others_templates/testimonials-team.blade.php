@extends('layouts.global_main')
@section('title') {{ $team->name_team }} - Productos Moisés @endsection
@section('urlcanonical','https://productosmoises.com/nosotros')
@section('description'){!! strip_tags(html_entity_decode(str_limit( $team->testimonial, 82))) !!}@endsection
@section('keywords', 'equipo moisés, productos moisés')
{{-- For FB Meta tags --}}
@section('ogtitle') {!! $team->name_team !!} | Productos Moisés @endsection
@section('ogurl') https://productosmoises.com/nosotros/equipo/{{ $team->slug }} @endsection
@section('ogimage') https://productosmoises.com/img/team/{{ $team->image }}@endsection
@section('ogdescription') {!! strip_tags(html_entity_decode(str_limit( $team->testimonial, 82))) !!}@endsection
{{-- End For FB Meta tags --}}
@section('content')
  <section id="parallax-phrase">
      <div class="parallax-container title-center-parallax">
        <h1 class="center-align title-parallax-view">Equipo Moisés</h1>
        <div class="parallax"><img src="/img/equipo-moises.jpg" alt="equipo-moises"></div>
      </div>
  </section>
  <section id="about" class="bg-white">
        <div class="content-wrapper">
          <div class="container">
            <div class="row">
              <div class="col s12 l12 m12">
                <div class="center-align">
                  <img class="img-avatar-team-admin" src="/img/team/{{ $team->image }}" alt="{{ $team->image_description }}">
                </div>
                <h5 class="center-align no-margin sub-title">{{ $team->job_title }}</h5>
                <h3 class="center-align title no-margin">{{ $team->name_team }}</h3>
                <div class="content-text-p">
                  <p class="justify-align color-dark-content">{!! html_entity_decode($team->testimonial) !!}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
  </section>
@endsection
