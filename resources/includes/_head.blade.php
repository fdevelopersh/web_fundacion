<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>@yield('title')</title>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<link rel="canonical" href="@yield('urlcanonical')"/>
<link rel="stylesheet" href="{{asset('/css/main.css')}}" type="text/css" />
